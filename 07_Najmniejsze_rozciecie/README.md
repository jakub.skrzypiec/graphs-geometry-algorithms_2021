# zad06
## Minimal disjoint of the graph Problem [Randomized algorithm]
### Given multiGraph G=(V,E), find minimal set of Edges (from E) so that when removed - the graph will be disjoined into two.

### Input 
    - Graph: multigraph G=(V,E) with V-vertexSet, E-edgeSet
### Output 
    - Number: minimal disjoint size
    - Set of Edges: edges to be removed

### Note
Merge Edges is an important operation in this algorithm.  
When merged edge E1 (joining vertices - V1 and V2), V2 is merged into V1:  
- edge E1 and all other edges joining V1 & V2 are removed, in order to avoid self loops,
- all other edges of V2 are assigned to V1

### Example:
A randomized algorithm:  
- randomly selects edge to be merged up until there are only 2 vertices left. There will be multiple edges joining remaining vertices V1 and V2. Combined degree of those edges is a tmp solution - it might not be optimal due to random selections of edges to merge.  
Given that - the algorithm is executed X times, and we seek the best solution out of the X-solutions.  
The higher the X, the higher the chance of obtaining the optimal solution.  
eg.

Input graph:
![xxx](IMG/7_1.png "xxx")  

Merge (e2)
![xxx](IMG/7_2.png "xxx")

Merge (e13)
![xxx](IMG/7_3.png "xxx")  

Merge (e6)
![xxx](IMG/7_4.png "xxx")  

Merge (e5)
![xxx](IMG/7_5.png "xxx")  

Merge (e10)
![xxx](IMG/7_6.png "xxx")  

Merge (e14)
![xxx](IMG/7_7.png "xxx")  

At this point, there are only 2 vertices left, so we can get the result. Since we were modifying the vertices and edges, we need to read the edges from original graph based on the edge indexes:  
![xxx](IMG/7_8.png "xxx")  