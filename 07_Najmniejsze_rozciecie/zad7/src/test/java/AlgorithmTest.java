import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AlgorithmTest {

    Algorithm alg;

    @BeforeClass
    public static void setup() {
        BasicConfigurator.configure();  // configure the logging (default)
    }
    @Before
    public void bla() {
        alg = new Algorithm();
    }


    /**
     * file01 - K3 (optimal solution = 2 edges)
     */
    @Test
    public void executeAlg_file01_test01() {
        // given fileName
        String fileName = "file1.txt";

        // when
        alg.executeAlg(fileName, 100);

        // then
        assertEquals(2, alg.getMinimalDisjointSize());
        assertEquals(2, alg.getMinimalDisjointEdges().size());
    }

    /**
     * file02 - K4 (optimal solution = 3 edges)
     */
    @Test
    public void executeAlg_file02_test01() {
        // given fileName
        String fileName = "file2.txt";
        // when
        alg.executeAlg(fileName, 100);
        // then
        assertEquals(3, alg.getMinimalDisjointSize());
        assertEquals(3, alg.getMinimalDisjointEdges().size());
    }

    /**
     * file03 - K5 (optimal solution = 4 edges)
     */
    @Test
    public void executeAlg_file03_test01() {
        // given fileName
        String fileName = "file3.txt";
        // when
        alg.executeAlg(fileName, 100);
        // then
        assertEquals(4, alg.getMinimalDisjointSize());
        assertEquals(4, alg.getMinimalDisjointEdges().size());
    }


    /**
     * file04 - custom graph (optimal solution = 3)
     */
    @Test
    public void executeAlg_file04_test01() {
        // given fileName
        String fileName = "file4.txt";
        // when
        alg.executeAlg(fileName, 100);
        // then
        assertEquals(3, alg.getMinimalDisjointSize());
        assertEquals(3, alg.getMinimalDisjointEdges().size());
    }
}
