package baseModel;

import java.util.Objects;

public class Edge {

    private static int nextId = 1;

    private Vertex vertex1;
    private Vertex vertex2;
    private int id;
    private int degree;     // degree of the Edge, if eg. = 2 -> there are two edges connecting vertex1 & vertex2

    public Edge() {
        this.id = nextId++;
        this.degree = 1;
    }
    public Edge(Vertex vertex1, Vertex vertex2) {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.id = nextId++;
        this.degree = 1;
    }

    public void incrementDegreeByOne() {
        this.degree++;
    }

    public static int getNextId() {
        return nextId;
    }
    public static void setNextId(int nextId) {
        Edge.nextId = nextId;
    }
    public Vertex getVertex1() {
        return vertex1;
    }
    public void setVertex1(Vertex vertex1) {
        this.vertex1 = vertex1;
    }
    public Vertex getVertex2() {
        return vertex2;
    }
    public void setVertex2(Vertex vertex2) {
        this.vertex2 = vertex2;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getDegree() {
        return degree;
    }
    public void setDegree(int degree) {
        this.degree = degree;
    }

    public void decreaseDegreeByOne() {
        if(this.degree>0){
            this.degree--;
        }
    }

    @Override
    public String toString() {
        return "Edge{" +
                "vertex1=" + vertex1 +
                ", vertex2=" + vertex2 +
                ", id=" + id +
                ", degree=" + degree +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return vertex1.equals(edge.vertex1) && vertex2.equals(edge.vertex2) ||
                vertex1.equals(edge.vertex2) && vertex2.equals(edge.vertex1);
    }
    @Override
    public int hashCode() {
        return Objects.hash(vertex1, vertex2);
    }



}
