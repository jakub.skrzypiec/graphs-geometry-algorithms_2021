package baseModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Vertex {

    private int x;
    private List<Edge> edges = new ArrayList<>();

    public Vertex () {}
    public Vertex(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public List<Edge> getEdges() {
        return edges;
    }
    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Vertex{x=" + x);
        sb.append(", edges=");
        for(int i=0; i<edges.size(); i++){
            sb.append("(id:"+edges.get(i).getId()+") {" + edges.get(i).getVertex1().getX() + "," +
                    edges.get(i).getVertex2().getX() + "}") ;
            if(i != edges.size()-1){
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return x == vertex.x;
    }
    @Override
    public int hashCode() {
        return Objects.hash(x);
    }

}
