package baseModel;

import java.util.ArrayList;
import java.util.List;

public class Multigraph {

    private List<Vertex> vertexList = new ArrayList<>();
    private List<Edge> edgeList = new ArrayList<>();

    public Multigraph() {}
    public Multigraph(List<Vertex> vertexList, List<Edge> edgeList) {
        this.vertexList = vertexList;
        this.edgeList = edgeList;
    }

    public List<Edge> getEdgeList() {
        return edgeList;
    }
    public void setEdgeList(List<Edge> edgeList) {
        this.edgeList = edgeList;
    }
    public List<Vertex> getVertexList() {
        return vertexList;
    }
    public void setVertexList(List<Vertex> vertexList) {
        this.vertexList = vertexList;
    }



    @Override
    public String toString() {
        return "Multigraph{" +
                "vertexList=" + vertexList +
                ", edgeList=" + edgeList +
                '}';
    }


}
