import org.apache.log4j.BasicConfigurator;

public class Zad7_Application {

    public static void main(String[] args) {
        BasicConfigurator.configure();  // configure the logging (default)

        // create alg instance and execute alg X-times;
        Algorithm alg = new Algorithm();
        alg.executeAlg("file2.txt", 1);
    }

}
