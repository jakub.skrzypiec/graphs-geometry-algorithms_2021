package utils;

import java.util.Random;

public class RandomUtils {

    public static int randomListIndex(int listSize) {
        Random random = new Random();
        int randomIndex = random.ints(0, listSize)
                .findFirst()
                .getAsInt();
        return randomIndex;
    }

}
