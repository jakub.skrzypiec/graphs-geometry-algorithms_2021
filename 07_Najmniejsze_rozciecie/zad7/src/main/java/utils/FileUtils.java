package utils;

import baseModel.Edge;
import baseModel.Multigraph;
import baseModel.Vertex;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    private static Logger logger = Logger.getLogger(FileUtils.class);

    /**
     * method that reads a Multigraph from input file
     */
    public static Multigraph fileToMultigraph(String fileName) {
        // vertexList
        List<Vertex> vertexList = new ArrayList<>();
        // edgeList
        List<Edge> edgeList = new ArrayList<>();

        // read vertexList
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            String[] lineArr;
            logger.info("READING MULTIGRAPH FROM FILE...");
            logger.info("READING MULTIGRAPH VERTEX LIST...");

            while ((line = br.readLine()) != null) {
                lineArr = line.split(":");
                if(lineArr.length != 2){
                    throw new IllegalArgumentException("Incorrect file format!"); // in each line there should be [Vertex:edge1,edge2,...]
                }
                String vertexStr = lineArr[0];
                // read the vertex
                int tmpVertex_id = Integer.parseInt(vertexStr);
                Vertex tmpVertex = new Vertex(tmpVertex_id);
                logger.debug("\tread potential vertex: " + tmpVertex);
                if (!vertexList.contains(tmpVertex)) {
                    vertexList.add(tmpVertex);
                    logger.info("\tadded VERTEX #: " + tmpVertex + " (not a duplicate Vertex)");
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }

        // edge vertexList
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            String[] lineArr;
            logger.info("READING MULTIGRAPH EDGES LIST...");

            while ((line = br.readLine()) != null) {
                lineArr = line.split(":");
                if(lineArr.length != 2){
                    throw new IllegalArgumentException("Incorrect file format!"); // in each line there should be [Vertex:edge1,edge2,...]
                }
                String vertexStr = lineArr[0];
                String edgesStr = lineArr[1];
                // read the vertex
                int tmpVertex1_id = Integer.parseInt(vertexStr);
                Vertex helperVertex1 = new Vertex(tmpVertex1_id);
                Vertex tmpVertex1 = vertexList.get(vertexList.indexOf(helperVertex1));
                // read the edges
                String[] edges = edgesStr.split(",");
                if(edges.length < 1){
                    throw new IllegalArgumentException("Incorrect file format!"); // There must be at leas one edge for earch vertex
                }
                for(String edge : edges) {
                    int tmpVertex2_id = Integer.parseInt(edge);
                    Vertex helperVertex2 = new Vertex(tmpVertex2_id);
                    Vertex tmpVertex2 = vertexList.get(vertexList.indexOf(helperVertex2));
                    Edge tmpEdge = new Edge(tmpVertex1, tmpVertex2);
                    if (!edgeList.contains(tmpEdge)){
                        edgeList.add(tmpEdge);
                        logger.info("\tadded EDGE #: (" + tmpEdge.getVertex1().getX() + "," + tmpEdge.getVertex2().getX()
                                + ") (not a duplicate Edge)");
                    } else {
                        tmpEdge = edgeList.get(edgeList.indexOf(tmpEdge));
                        tmpEdge.incrementDegreeByOne();
                        logger.info("\tadded EDGE #: (" + tmpEdge.getVertex1().getX() + "," + tmpEdge.getVertex2().getX()
                                + ") (duplicate Edge)");
                    }
                }
            }
//            System.out.println(vertexList);
//            System.out.println(edgeList);
            // since all edges were scanned exactly twice = reduce every edge degree
            for(Edge e : edgeList) {
                e.decreaseDegreeByOne();
                // for each edge add edge info to it's vertex1 & vertex2
                e.getVertex1().getEdges().add(e);
                e.getVertex2().getEdges().add(e);
            }
        } catch(IOException e){
            e.printStackTrace();
        }

        Multigraph multigraph = new Multigraph(vertexList, edgeList);
        return multigraph;
    }

}
