import baseModel.Edge;
import baseModel.Multigraph;
import baseModel.Vertex;
import org.apache.log4j.Logger;
import utils.FileUtils;
import utils.RandomUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {
    private static Logger logger = Logger.getLogger(Algorithm.class);

    private Multigraph originalGraph;
    private Multigraph graph;
    private int minimalDisjointSize = Integer.MAX_VALUE;
    private int tmpMinimalDisjointSize;
    private List<Edge> minimalDisjointEdges = new ArrayList<>();
    private List<Edge> tmpMinimalDisjointEdges = new ArrayList<>();

    private Map<Integer, Edge> idToEdgeMap = new HashMap<>();

    public Algorithm() {}


    private void initMultigraph(String fileName) {
        originalGraph = FileUtils.fileToMultigraph(fileName);
        initGraphcopy(fileName);
    }
    private void copyOriginalgraph() {
        List<Edge> edgeList = new ArrayList<>(originalGraph.getEdgeList());
    }
    /**
     * executes alg given number of 'times'
     */
    public void executeAlg(String fileName, int times) {
        initMultigraph(fileName);

        // put (id, originalEdge) to map for later usage (reding solution edgeList)
        for(Edge edge : originalGraph.getEdgeList()) {
            Edge newEdge = new Edge(edge.getVertex1(), edge.getVertex2());
            newEdge.setId(edge.getId());
            idToEdgeMap.put(edge.getId(), newEdge);
        }

        // execute alg given number of TIMES
        for(int i=0; i<times; i++){
            executeCoreAlg(fileName);
        }

        // if tmp solution is better than global --> assign new global solution
        if(tmpMinimalDisjointSize < minimalDisjointSize){
            minimalDisjointSize = tmpMinimalDisjointSize;   // size
            // solution
            minimalDisjointEdges.clear();
            for(Edge e : tmpMinimalDisjointEdges) {
                minimalDisjointEdges.add(idToEdgeMap.get(e.getId()));
            }
        }

        logger.info("(global) MINIMAL DISJOINT SET SIZE: " + minimalDisjointSize);
        logger.info("(global) MINIMAL DISJOINT EDGE SET: " + minimalDisjointEdges);
        minimalDisjointEdges.forEach(e -> System.out.println("[" + e.getVertex1().getX() + "," + e.getVertex2().getX() + "]"));

        System.out.println(graph);
    }

    /**
     * core alg
     */
    public void executeCoreAlg(String fileName) {
        initGraphcopy(fileName);
        int counter = originalGraph.getVertexList().size();
        for(int i=2; i<counter; i++) {
            int range = graph.getEdgeList().size();
            int randomEdgeIndex = RandomUtils.randomListIndex(range);
            this.mergeEdge(graph.getEdgeList().get(randomEdgeIndex));
        }

        // tmp-solution & it's size
        tmpMinimalDisjointSize = graph.getEdgeList().stream().mapToInt(e -> e.getDegree()).sum();
        // add edges to solution
        tmpMinimalDisjointEdges.clear();
        tmpMinimalDisjointEdges = new ArrayList<>(graph.getEdgeList());

        logger.info("(tmp) MINIMAL DISJOINT SET SIZE: " + tmpMinimalDisjointSize);
        logger.info("(tmp) MINIMAL DISJOINT EDGE SET: " + tmpMinimalDisjointEdges);
    }

    /**
     * method to merge Edge -> it takes 2 vertices, merges them into 1 and removes input Edge (or multiple edges
     * connecting v1 & v2 if the exist)
     */
    public void mergeEdge(Edge edge) {
        logger.info("before merge");
        System.out.println("graph.edges.size: " + graph.getEdgeList().size());
        System.out.println("graph.vert.size: " + graph.getVertexList().size());
        System.out.println(graph);
        logger.info("MERGING EDGE:" + edge.getId());
        Vertex vertex1 = edge.getVertex1();
        Vertex vertex2 = edge.getVertex2();
        // remove multiple edges connecting v1 & v2 (if they exist)

        while (vertex1.getEdges().contains(edge) || vertex2.getEdges().contains(edge)) {
            vertex1.getEdges().remove(edge);
            vertex2.getEdges().remove(edge);
        }

        List<Edge> selfLoops = new ArrayList<>();
        // replace vertex2 with vertex1 in all connections (Edge data)
        for (Edge graphEdge : graph.getEdgeList()) {
            if (graphEdge.getVertex1() == vertex2) {
                graphEdge.setVertex1(vertex1);
            } else if (graphEdge.getVertex2() == vertex2) {
                graphEdge.setVertex2(vertex1);
            }

            // remove if self-loop
            if(graphEdge.getVertex1().getX() == graphEdge.getVertex2().getX()){
                selfLoops.add(graphEdge);
            }
        }
        graph.getEdgeList().removeAll(selfLoops);

        // assing all Edges from vertex2 to vertex1
        for(Edge vertex2_edge : vertex2.getEdges()) {
            vertex1.getEdges().add(vertex2_edge);
        }

        // remove edge from the graph
        graph.getVertexList().remove(vertex2);
        graph.getEdgeList().remove(edge);
    }


    public int getMinimalDisjointSize() {
        return minimalDisjointSize;
    }
    public List<Edge> getMinimalDisjointEdges() {
        return minimalDisjointEdges;
    }


    private void initGraphcopy(String fileName) {
        graph = FileUtils.fileToMultigraph(fileName);
        // copy original Edge ids;
        for (int i=0; i<graph.getEdgeList().size(); i++) {
            graph.getEdgeList().get(i).setId(originalGraph.getEdgeList().get(i).getId());
        }
    }

}
