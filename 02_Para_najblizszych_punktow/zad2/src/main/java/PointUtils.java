import java.util.ArrayList;
import java.util.Collections;

public class PointUtils {

    // simple print
    public static <T> void printPointList(String listName, ArrayList<T> pointList){
        System.out.print(listName + ": ");
        for(T point : pointList){
            System.out.print(point + ", ");
        }
        System.out.println();
    }

    // sort by X/Y
    public static ArrayList<Point> sortPointListByX(ArrayList<Point> pointListByX) {
        Collections.sort(pointListByX, (p1, p2) -> {   // lambda Comparator
            if ((p1.getX() < p2.getX()) || (p1.getX() == p2.getX() && p1.getY() < p2.getY()))
                return -1;
            else if (p1.getX() > p2.getX())
                return 1;
            else
                return 0;
        });
        return pointListByX;
    }
    public static ArrayList<Point> sortPointListByY(ArrayList<Point> pointListByY) {
        Collections.sort(pointListByY, (p1, p2) -> {   // lambda Comparator
            if ((p1.getY() < p2.getY()) || (p1.getY() == p2.getY() && p1.getX() < p2.getX()))
                return -1;
            else if (p1.getY() > p2.getY())
                return 1;
            else
                return 0;
        });
        return pointListByY;
    }

    // compare two points by X (then Y if both X are equal)
    public static int compareTwoPoints(Point p1, Point p2) {
        if ((p1.getX() < p2.getX()) || (p1.getX() == p2.getX() && p1.getY() < p2.getY())) {
            return -1;
        } else if ((p1.getX() > p2.getX()) || (p1.getX() == p2.getX() && p1.getY() > p2.getY())) {
            return 1;
        } else {
            return 0;
        }
    }

    // calculate distance between given 2 Points
    public static double calculateDistance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p2.getX() - p1.getX(), 2) + Math.pow(p2.getY() - p1.getY(), 2));
    }


}
