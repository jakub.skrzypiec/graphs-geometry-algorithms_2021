import java.util.ArrayList;

public class Zad2_Application {
    public Zad2_Application() {}

    // point list (S)
    ArrayList<Point> s = new ArrayList<>();

    // copy of 'pointList' sorted by X
    ArrayList<Point> sx = new ArrayList<>();
    // copy of 'pointList' sorted by Y
    ArrayList<Point> sy = new ArrayList<>();

    // point separating s1 from s2 (separating point is included in s1)
    int separationPointIndex;
    Point separationPoint;

    // subsets of Sx
    ArrayList<Point> s1x = new ArrayList<>();
    ArrayList<Point> s2x = new ArrayList<>();
    // subsets of Sy
    ArrayList<Point> s1y = new ArrayList<>();
    ArrayList<Point> s2y = new ArrayList<>();

    // shortest distance between 2 points in s1 and s2 subsets (and Points)
    double s1_shortestDistance;
    double s2_shortestDistance;
    double s1s2_minShortestDistance;  // == min(s1_shortest, s2_shortest)
    int shortestDistanceSubset;  // later set to either '1' or '2' -- where the min was
    Point s1_shortest_p1;
    Point s1_shortest_p2;
    Point s2_shortest_p1;
    Point s2_shortest_p2;
    Point s1s2_shortest_p1;
    Point s1s2_shortest_p2;

    // s3 - set of points which X is in 'minShortestDistance' range from separatingPoint X
    ArrayList<Point> s3 = new ArrayList<>();
    // s3_char - array list containing s3.size() elements & containing either char '1'--S3_1 or '2'--S3_2
    ArrayList<Character> s3_char = new ArrayList<>();
    ArrayList<Point> s3_1 = new ArrayList<>();
    ArrayList<Point> s3_2 = new ArrayList<>();
    // shortest distance between 2 points in s3 (and Points)
    double s3_shortestDistance;
    Point s3_shortest_p1;
    Point s3_shortest_p2;

    // global shortest distance and 2 Points
    double s_shortestDistance;
    Point s_shortest_p1;
    Point s_shortest_p2;


    public static void main(String[] args) {
        Zad2_Application zad2_app = new Zad2_Application();

        ArrayList<Point> s = initializeSFromFile("file1.txt");  // read points from file
        zad2_app.sortSets(s);    // sort Sx and Sy - once;
        zad2_app.findGlobalShortestDistanceInSetOfGivenPoints(s);   // find globalShortestDistance
    }

    public static ArrayList<Point> initializeSFromFile(String fileName) {
        ArrayList<Point> s = FileUtils.fileToPointList(fileName);     // read point list
        if(s.size() == 0){
            System.out.println("Error! Failed to load the points from file - check file");
            return null;
        }
        return s;
    }

    // sort S by X and by Y to subsets (Sx and Sy)
    public void sortSets(ArrayList<Point> pointList) {
        this.s = pointList;
        this.sx = PointUtils.sortPointListByX(new ArrayList(this.s));    // sx - sorted by X
        this.sy = PointUtils.sortPointListByY(new ArrayList(this.s));    // sy - sorted by Y
        PointUtils.printPointList("Sx", this.sx);
        PointUtils.printPointList("Sy", this.sy);
        System.out.println();
    }

    ////////////
    public double findGlobalShortestDistanceInSetOfGivenPoints(ArrayList<Point> pointList) {
        this.s = pointList;
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$");
        PointUtils.printPointList("S", this.s);

        splitPointList(this.sx);
        PointUtils.printPointList("S1x", this.s1x);
        PointUtils.printPointList("S2x", this.s2x);
        PointUtils.printPointList("S1y", this.s1y);
        PointUtils.printPointList("S2y", this.s2y);
        System.out.println();

        // find shortestDistance in s1 and s2;
        if(this.s1x.size() < 4 || this.s2x.size() < 4){
            this.s1_shortestDistance = findShortestDistanceInPointList_naive(this.s1x, "s1");
            this.s2_shortestDistance = findShortestDistanceInPointList_naive(this.s2x, "s2");
        } else {
            // recursion
            this.s1_shortestDistance = findShortestDistanceInPointList(this.s1x, this.s1y, "s1");
            this.s2_shortestDistance = findShortestDistanceInPointList(this.s2x, this.s2y, "s2");
        }
        this.s1s2_minShortestDistance = selectShortestDistance();

        System.out.println("Shortest distance in s1: " + this.s1_shortestDistance + " - between points: "
                + this.s1_shortest_p1 + " and " + this.s1_shortest_p2);
        System.out.println("Shortest distance in s2: " + this.s2_shortestDistance + " - between points: "
                + this.s2_shortest_p1 + " and " + this.s2_shortest_p2);
        System.out.println("Min shortest distance (from s1 or s2): " + this.s1s2_minShortestDistance + " - between points: "
                + this.s1s2_shortest_p1 + " and " + this.s1s2_shortest_p2 + "\n");

        // add points to s3_1 and s3_2
        addPointsToS3_subsets();
        PointUtils.printPointList("S3", this.s3);
        PointUtils.printPointList("S3_char", this.s3_char);
        PointUtils.printPointList("S3_1", this.s3_1);
        PointUtils.printPointList("S3_2", this.s3_2);

        // calculate shortestDistance in S3 = (S3_1 x S3_2)
        this.s3_shortestDistance = findShortestDistanceIn_s3();
        System.out.println("Shortest distance in s3: " + this.s3_shortestDistance + " - between points: " + this.s3_shortest_p1
                + " and " + this.s3_shortest_p2 + "\n");

        // find the global shortest distance
        findGlobalShortestDistance();
        System.out.println("GLOBAL Shortest distance: " + this.s_shortestDistance + " - between points: " + this.s_shortest_p1
                + " and " + this.s_shortest_p2 + "\n#############################");

        // return the shortest distance
        return this.s_shortestDistance;
    }
    ////////////

    // split pointList
    public void splitPointList(ArrayList<Point> pointList) {
        separationPointIndex = pointList.size() / 2;
        separationPoint = pointList.get(separationPointIndex - 1);

        s1x.addAll(sx.subList(0, separationPointIndex));            // s1x = first 'half' of the sx  -- [0,sepPtIdx)
        s2x.addAll(sx.subList(separationPointIndex, sx.size()));    // s2x = second 'half' of the sx -- [sepPtIdx, sx.size)

        // s1y,s2y
        for (int i = 0; i < sy.size(); i++) {
            // lower and separation point to s1y, higher to s2y
            if (PointUtils.compareTwoPoints(sy.get(i), separationPoint) == 1) {
                s2y.add(sy.get(i));
            } else {
                s1y.add(sy.get(i));
            }
        }
    }

    // findShortestDistanceInPointList and indicate 2 points (with that distance)
    public double findShortestDistanceInPointList_naive(ArrayList<Point> pointList, String subsetName) {
        if (pointList.size() < 2 || pointList.get(0) == null || pointList.get(1) == null) {
            return -1;
        }
        // initialize the shortest
        double shortestDistance = PointUtils.calculateDistance(pointList.get(0), pointList.get(1));
        if ("s1".equalsIgnoreCase(subsetName)) {
            s1_shortest_p1 = pointList.get(0);
            s1_shortest_p2 = pointList.get(1);
        } else if ("s2".equalsIgnoreCase(subsetName)) {
            s2_shortest_p1 = pointList.get(0);
            s2_shortest_p2 = pointList.get(1);
        }

        double tmpDistance;
        for (int i = 0; i < pointList.size(); i++) {
            for (int j = 0; j < pointList.size(); j++) {
                if (i == j) {   // do not compare point to the same point (to avoid distance == 0.0)
                    continue;
                }
                tmpDistance = PointUtils.calculateDistance(pointList.get(i), pointList.get(j));
                if (tmpDistance < shortestDistance) {
                    shortestDistance = tmpDistance;
                    // based on provided subsetName -- change static 'Point' variables
                    if ("s1".equalsIgnoreCase(subsetName)) {
                        s1_shortest_p1 = pointList.get(i);
                        s1_shortest_p2 = pointList.get(j);
                    } else if ("s2".equalsIgnoreCase(subsetName)) {
                        s2_shortest_p1 = pointList.get(i);
                        s2_shortest_p2 = pointList.get(j);
                    }
                }
            }
        }
        return shortestDistance;
    }

    public double findShortestDistanceInPointList(ArrayList<Point> pointListByX, ArrayList<Point> pointListByY,  String subsetName) {
        Zad2_Application tmpApp = new Zad2_Application();
        tmpApp.s = pointListByX;
        tmpApp.sx = pointListByX;
        tmpApp.sy = pointListByY;

        double shortestDistance = tmpApp.findGlobalShortestDistanceInSetOfGivenPoints(pointListByX);

        if ("s1".equalsIgnoreCase(subsetName)) {
            s1_shortest_p1 = tmpApp.s_shortest_p1;
            s1_shortest_p2 = tmpApp.s_shortest_p2;
        } else if ("s2".equalsIgnoreCase(subsetName)) {
            s2_shortest_p1 = tmpApp.s_shortest_p1;
            s2_shortest_p2 = tmpApp.s_shortest_p2;
        }

        return shortestDistance;
    }

    //
    public double selectShortestDistance() {
        double shortestDistance = Math.min(s1_shortestDistance, s2_shortestDistance);
        if (shortestDistance == s1_shortestDistance) {
            shortestDistanceSubset = 1;
            s1s2_shortest_p1 = s1_shortest_p1;
            s1s2_shortest_p2 = s1_shortest_p2;
        } else {
            shortestDistanceSubset = 2;
            s1s2_shortest_p1 = s2_shortest_p1;
            s1s2_shortest_p2 = s2_shortest_p2;
        }
        return shortestDistance;
    }

    // add points to s3_1 and s3_2, and S3
    public void addPointsToS3_subsets() {
        double xDiff;
        for (Point point : s1y) {
            xDiff = Math.abs(separationPoint.getX() - point.getX());
            if (xDiff <= s1s2_minShortestDistance) {
                s3.add(point);
                s3_char.add('1');
                s3_1.add(point);
            }
        }
        for (Point point : s2y) {
            xDiff = Math.abs(separationPoint.getX() - point.getX());
            if (xDiff <= s1s2_minShortestDistance) {
                s3_2.add(point);
                s3_char.add('2');
                s3.add(point);
            }
        }
    }

    // calculate shortestDistance in S3 = (S3_1 x S3_2)
    // for simplification:
    //  for each point from s3, check distances between that point and next 7 points (sorted by Y)
    public double findShortestDistanceIn_s3() {
        if (s3_1 == null || s3_2 == null || s3_1.size() <= 0 || s3_2.size() <= 0) {
            return -1;
        }
        // shortest overall (S3_1->S3_2)x(S3_2->S3_1)
        double shortestS3 = PointUtils.calculateDistance(s3_1.get(0), s3_2.get(0));
        s3_shortest_p1 = s3_1.get(0);
        s3_shortest_p2 = s3_2.get(0);
        int anotherSetCounter = 0;
        for (int i = 0; i < s3.size(); i++) {
            Point firstPoint = s3.get(i);
            char firstPointChar = s3_char.get(i);

            for (int j = i + 1; j < i + 7; j++) {
                // if size of another s3 subset is reached OR 4 point from another s3 subset were checked
                if (j >= s3.size() || anotherSetCounter >= 4) {
                    break;
                }
                // if the point is from another s3 subset (s3_1 vs s3_2)
                if (s3_char.get(j) != firstPointChar) {
                    Point secondPoint = s3.get(j);
                    // check whether the distance is shorter
                    double tmpDistance = PointUtils.calculateDistance(firstPoint, secondPoint);
                    if (tmpDistance < shortestS3) {
                        shortestS3 = tmpDistance;
                        s3_shortest_p1 = firstPoint;
                        s3_shortest_p2 = secondPoint;
                    }
                }
                anotherSetCounter++;
            }
            anotherSetCounter = 0;
        }
        return shortestS3;
    }

    public void findGlobalShortestDistance() {
        // 2 points with shortest distance in between are either in S1 or S2
        if (s1s2_minShortestDistance < s3_shortestDistance || s3_shortestDistance == -1) {
            s_shortestDistance = s1s2_minShortestDistance;
            s_shortest_p1 = s1s2_shortest_p1;
            s_shortest_p2 = s1s2_shortest_p2;
        // 2 points with shortest distance in between are in S3
        } else {
            s_shortestDistance = s3_shortestDistance;
            s_shortest_p1 = s3_shortest_p1;
            s_shortest_p2 = s3_shortest_p2;
        }
    }

}
