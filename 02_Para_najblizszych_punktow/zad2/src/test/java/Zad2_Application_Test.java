import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class Zad2_Application_Test {

    public void test_isTheShortestDistanceCorrect(String fileName, double distance) {
        Zad2_Application zad2_app = new Zad2_Application();

        ArrayList<Point> s = Zad2_Application.initializeSFromFile(fileName); // read points from file
        zad2_app.sortSets(s);    // sort Sx and Sy - once;

        double shortestDistance = zad2_app.findGlobalShortestDistanceInSetOfGivenPoints(s);
        assertEquals(distance, shortestDistance, 0.01);
    }


    @Test
    public void test_shortestDistance01() {
        test_isTheShortestDistanceCorrect("file1.txt", 2.00);
    }

    @Test
    public void test_shortestDistance02() {
        test_isTheShortestDistanceCorrect("file2.txt", 1.41);
    }

    @Test
    public void test_shortestDistance03() {
        test_isTheShortestDistanceCorrect("file3.txt", 1.00);
    }

    @Test
    public void test_shortestDistance04() {
        test_isTheShortestDistanceCorrect("file4.txt", 2.00);
    }

    @Test
    public void test_shortestDistance05() {
        test_isTheShortestDistanceCorrect("file5.txt", 1.41);
    }

    @Test
    public void test_shortestDistance06() {
        test_isTheShortestDistanceCorrect("file6.txt", 3.00);
    }

    @Test
    public void test_shortestDistance_01() {
        test_isTheShortestDistanceCorrect("file_01.txt", 1.00);
    }
    @Test
    public void test_shortestDistance_02() {
        test_isTheShortestDistanceCorrect("file_02.txt", 1.00);
    }
    @Test
    public void test_shortestDistance_03() {
        test_isTheShortestDistanceCorrect("file_03.txt", 0.00);
    }
    @Test
    public void test_shortestDistance_04() {
        test_isTheShortestDistanceCorrect("file_04.txt", 1.00);
    }


}
