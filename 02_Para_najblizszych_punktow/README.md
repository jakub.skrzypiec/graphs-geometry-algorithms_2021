# zad02
## Shortest distance
### Find shortest distance between 2 points [Divide-and-conquer algorithm]

### Input
    - Set of points 
### Output
    - One value: shortest distance

### Example
![xxx](IMG/2_1.png "xxx")
![xxx](IMG/2_2.png "xxx")
![xxx](IMG/2_3.png "xxx")
![xxx](IMG/2_4.png "xxx")
![xxx](IMG/2_5.png "xxx")
![xxx](IMG/2_6.png "xxx")
![xxx](IMG/2_7.png "xxx")
