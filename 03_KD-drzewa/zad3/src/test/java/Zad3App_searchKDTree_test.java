import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * search KD_tree tests - verify search results
 */
public class Zad3App_searchKDTree_test {

    private PointListBuilder pb = new PointListBuilder();
    private final static String SR_SIZE_ER = "searchResult incorrect size";
    private final static String SR_POINT_ER = "searchResult does not contain all the points it should contain";

    // load points from 'fileName' to List (zad3_app.s), create kdTree (zad3_app.node) out of that list
    // search previously build KD-tree
    // check which points are inside searchArea
    public static List<Point> KDTreeSearchResults(String fileName, Area searchArea) {
        // init Zad3_app
        Zad3_Application zad3_app = Zad3_Application.initZad3App(fileName);

        // search result
        List<Point> searchResult = zad3_app.searchKD_Tree(zad3_app.node, searchArea);
        // print search results
        PointUtils.printPointList("search result: ", searchResult);

        return searchResult;
    }

    /**
     * file0
     */
    @Test
    public void searchKDTree_test01() {
        // given String fileName and Area searchArea
        String fileName = "file0.txt";
        Area searchArea = new Area(0,0,10,10);

        // when invoking 'KD_Tree search' method
        List<Point> searchResult = KDTreeSearchResults(fileName, searchArea);

        // then verify search results
        assertEquals(8, searchResult.size());
        assertTrue(searchResult.containsAll(pb
                .add(1,1)
                .add(2,2)
                .add(3,3)
                .add(4,4)
                .add(6,3)
                .add(7,2)
                .add(8,1)
                .add(9,0)
                .build()));
    }

    @Test
    public void searchKDTree_test02() {
        List<Point> searchResult = KDTreeSearchResults("file0.txt", new Area(0,0,4,4));

        assertEquals(SR_SIZE_ER, 4, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(1,1)
                .add(2,2)
                .add(3,3)
                .add(4,4)
                .build()));
    }

    @Test
    public void searchKDTree_test03() {
        List<Point> searchResult = KDTreeSearchResults("file0.txt", new Area(4,1,6,5));

        assertEquals(SR_SIZE_ER, 2, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(4,4)
                .add(6,3)
                .build()));
    }

    @Test
    public void searchKDTree_test04() {
        List<Point> searchResult = KDTreeSearchResults("file0.txt", new Area(8,-1, 9,0));

        assertEquals(SR_SIZE_ER, 1, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(9,0)
                .build()));
    }

    @Test
    public void searchKDTree_test05() {
        List<Point> searchResult = KDTreeSearchResults("file0.txt", new Area(3,0,7,7));

        assertEquals(SR_SIZE_ER, 4, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(3,3)
                .add(4,4)
                .add(6,3)
                .add(7,2)
                .build()));
    }

    @Test
    public void searchKDTree_test06() {
        List<Point> searchResult = KDTreeSearchResults("file0.txt", new Area(-1, -1,1,1));

        assertEquals(SR_SIZE_ER, 1, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(1,1)
                .build()));
    }

    @Test
    public void searchKDTree_test07() {
        List<Point> searchResult = KDTreeSearchResults("file0.txt", new Area(-2, 2,4,4));

        assertEquals(SR_SIZE_ER, 3, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(2,2)
                .add(3,3)
                .add(4,4)
                .build()));
    }

    /**
     * file1
     */
    // test whether it finds all the points (count: 5)
    @Test
    public void searchKDTree_test08() {
        List<Point> searchResult = KDTreeSearchResults("file1.txt", new Area(0, 0,10,10));

        assertEquals(SR_SIZE_ER, 5, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(1,1)
                .add(2,2)
                .add(3,3)
                .add(4,4)
                .add(5,5)
                .build()));
    }

    /**
     * file2
     */
    // test whether it finds all the points (count: 7)
    @Test
    public void searchKDTree_test09() {
        List<Point> searchResult = KDTreeSearchResults("file2.txt", new Area(0, 0,10,10));

        assertEquals(SR_SIZE_ER, 7, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(1,1)
                .add(2,2)
                .add(3,3)
                .add(4,4)
                .add(9,9)
                .add(8,8)
                .add(7,7)
                .build()));
    }

    /**
     * file3
     */
    // test whether it finds all the points (count: 9)
    @Test
    public void searchKDTree_test10() {
        List<Point> searchResult = KDTreeSearchResults("file3.txt", new Area(0, 0,10,10));

        assertEquals(SR_SIZE_ER, 9, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(1,1)
                .add(2,2)
                .add(3,3)
                .add(4,4)
                .add(9,9)
                .add(8,8)
                .add(7,7)
                .add(6,6)
                .add(5,5)
                .build()));
    }

    /**
     * file4
     */
    // expect 2 points - (2,4) and (3,3)
    @Test
    public void searchKDTree_test11() {
        List<Point> searchResult = KDTreeSearchResults("file4.txt", new Area(1.8, 2.5, 3.2, 5.8));

        assertEquals(SR_SIZE_ER, 2, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(2,4)
                .add(3,3)
                .build()));
    }
    // expect all the points (count:5)
    @Test
    public void searchKDTree_test12() {
        List<Point> searchResult = KDTreeSearchResults("file4.txt", new Area(0,0,6,6));

        assertEquals(SR_SIZE_ER, 5, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(5,1)
                .add(4,2)
                .add(3,3)
                .add(2,4)
                .add(5,1)
                .build()));
    }
    // expect 0 points (count: 0)
    @Test
    public void searchKDTree_test13() {
        List<Point> searchResult = KDTreeSearchResults("file4.txt", new Area(3.5,3.5, 4.5,4.5));

        assertEquals(SR_SIZE_ER, 0, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .build()));
    }

    /**
     * file5
     */
    // expect 4 points - (2,2), (2,3), (3,2), (3,3)
    @Test
    public void searchKDTree_test14() {
        List<Point> searchResult = KDTreeSearchResults("file5.txt", new Area(1.5, 1.5, 3.5, 3.5));

        assertEquals(SR_SIZE_ER, 4, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(2,2)
                .add(2,3)
                .add(3,2)
                .add(3,3)
                .build()));
    }
    // expect all points (count:16)
    @Test
    public void searchKDTree_test15() {
        List<Point> searchResult = KDTreeSearchResults("file5.txt", new Area(0.5, 0.5, 4.5, 4.5));

        assertEquals(SR_SIZE_ER, 16, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .add(1,1).add(1,2).add(1,3).add(1,4)
                .add(2,1).add(2,2).add(2,3).add(2,4)
                .add(3,1).add(3,2).add(3,3).add(3,4)
                .add(4,1).add(4,2).add(4,3).add(4,4)
                .build()));
    }
    // expect 0 point (count:0)
    @Test
    public void searchKDTree_test16() {
        List<Point> searchResult = KDTreeSearchResults("file5.txt", new Area(-1,-1, 0.9,5.5));

        assertEquals(SR_SIZE_ER, 0, searchResult.size());
        assertTrue(SR_POINT_ER, searchResult.containsAll(pb
                .build()));
    }


}
