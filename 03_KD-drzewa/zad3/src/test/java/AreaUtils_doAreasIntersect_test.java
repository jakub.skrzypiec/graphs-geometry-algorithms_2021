import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AreaUtils_doAreasIntersect_test {

    /**
     * 2 Areas Do not intersect at all
     */
    @Test
    public void areaUtils_doAreasIntersect_expectDoNotIntersectAtAll_test01(){
        // given 2 Areas
        Area area1 = new Area(0,0, 4,4);
        Area area2 = new Area(5,0, 9,4);

        // when invoking 'AreaUtils.doAreasIntersect(area1, area2)' method
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);

        // then
        assertFalse(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoNotIntersectAtAll_test02(){
        Area area1 = new Area(0,6, 4,10);
        Area area2 = new Area(0,0, 4,4);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertFalse(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoNotIntersectAtAll_test03(){
        Area area1 = new Area(7,1, 11,5);
        Area area2 = new Area(1,1, 5,5);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertFalse(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoNotIntersectAtAll_test04(){
        Area area1 = new Area(2,2, 6,6);
        Area area2 = new Area(2,10, 6,14);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertFalse(doAreasIntersect);
    }


    /**
     * 2 Areas Do intersect - one line
     */
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect1Line_test01(){
        Area area1 = new Area(0,0, 4,4);
        Area area2 = new Area(3,0, 7,4);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect1Line_test02(){
        Area area1 = new Area(5,5, 9,9);
        Area area2 = new Area(5,1, 9,6);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect1Line_test03(){
        Area area1 = new Area(5,5, 10,10);
        Area area2 = new Area(0,0, 6,12);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect1Line_test04(){
        Area area1 = new Area(4,4, 8,8);
        Area area2 = new Area(1,3, 12,20);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }


    /**
     * 2 Areas Do intersect - two lines
     */
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect2Lines_test01(){
        Area area1 = new Area(1,5, 6,10);
        Area area2 = new Area(4,1, 7,14);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect2Lines_test02(){
        Area area1 = new Area(3,3, 7,7);
        Area area2 = new Area(0,0, 4,4);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect2Lines_test03(){
        Area area1 = new Area(3,0, 7,4);
        Area area2 = new Area(0,3, 4,7);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersect2Lines_test04(){
        Area area1 = new Area(0,0, 4,4);
        Area area2 = new Area(2,2, 6,6);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }


    /**
     * 2 Areas Do intersect - one Area is completely inside the second Area
     */
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersectAreaInArea_test01(){
        Area area1 = new Area(2,2, 3,3);
        Area area2 = new Area(0,0, 4,4);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }
    @Test
    public void areaUtils_doAreasIntersect_expectDoIntersectAreaInArea_test02(){
        Area area1 = new Area(1,1, 9,9);
        Area area2 = new Area(5,5, 7,7);
        boolean doAreasIntersect = AreaUtils.doAreasIntersect(area1, area2);
        assertTrue(doAreasIntersect);
    }

}
