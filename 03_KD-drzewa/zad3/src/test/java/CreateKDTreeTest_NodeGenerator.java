/**
 * Class to generate specific structure of Nodes (hard coded)
 * It represents KD_Tree build ot ouf points (1,1), (2,2), (3,3), (4,4), (6,3), (7,2), (8,1), (9,0):
 *
 *                L01 (ROOT OF THE KD_TREE)
 *               /                        \
 *             L02                       L03
 *         /          \              /          \
 *      L04          L05           L06         L07
 *   /      \      /      \     /      \    /       \
 *  L08   L09     L10   L11    L12   L13    L14   L15
 * (1,1) (2,2)   (3,3) (4,4)  (6,3) (7,2)  (8,1) (9,0)
 */

public class CreateKDTreeTest_NodeGenerator {
    private static NodeBuilder nb = new NodeBuilder();

    // leaf Nodes
    private static Node l8 = nb
            .withPoint(new Point(1,1))
            .withArea(new Area(-Double.MAX_VALUE, -Double.MAX_VALUE, 1, 2))
            .build();
    private static Node l9 = nb
            .withPoint(new Point(2,2))
            .withArea(new Area(1, -Double.MAX_VALUE, 4, 2))
            .build();
    private static Node l10 = nb
            .withPoint(new Point(3,3 ))
            .withArea(new Area(-Double.MAX_VALUE, 2, 3, Double.MAX_VALUE))
            .build();
    private static Node l11 = nb
            .withPoint(new Point(4,4))
            .withArea(new Area(3, 2, 4, Double.MAX_VALUE))
            .build();
    private static Node l12 = nb
            .withPoint(new Point(8,1))
            .withArea(new Area(4, -Double.MAX_VALUE, 8, 1))
            .build();
    private static Node l13 = nb
            .withPoint(new Point(9,0))
            .withArea(new Area(8, -Double.MAX_VALUE, Double.MAX_VALUE, 1))
            .build();
    private static Node l14 = nb
            .withPoint(new Point(6,3))
            .withArea(new Area(4, 1, 6, Double.MAX_VALUE))
            .build();
    private static Node l15 = nb
            .withPoint(new Point(7,2))
            .withArea(new Area(6, 1, Double.MAX_VALUE, Double.MAX_VALUE))
            .build();


    // leaf Parent Nodes (root grandchildren)
    private static Node l4 = nb
            .withArea(new Area(-Double.MAX_VALUE, -Double.MAX_VALUE, 4,  2))
            .withLeftSon(l8)
            .withRightSon(l9)
            .withIsXSplit(true)
            .withIsLeaf(false)
            .build();
    private static Node l5 = nb
            .withArea(new Area(-Double.MAX_VALUE, 2, 4,  Double.MAX_VALUE))
            .withLeftSon(l10)
            .withRightSon(l11)
            .withIsXSplit(true)
            .withIsLeaf(false)
            .build();
    private static Node l6 = nb
            .withArea(new Area(4, -Double.MAX_VALUE, Double.MAX_VALUE,  1))
            .withLeftSon(l12)
            .withRightSon(l13)
            .withIsXSplit(true)
            .withIsLeaf(false)
            .build();
    private static Node l7 = nb
            .withArea(new Area(4, 1, Double.MAX_VALUE,  Double.MAX_VALUE))
            .withLeftSon(l14)
            .withRightSon(l15)
            .withIsXSplit(true)
            .withIsLeaf(false)
            .build();

    // leaf Parent Parent Nodes (root children)
    private static Node l2 = nb
            .withArea(new Area(-Double.MAX_VALUE, -Double.MAX_VALUE, 4,  Double.MAX_VALUE))
            .withLeftSon(l4)
            .withRightSon(l5)
            .withIsXSplit(false)
            .withIsLeaf(false)
            .build();
    private static Node l3 = nb
            .withArea(new Area(4, -Double.MAX_VALUE, Double.MAX_VALUE,  Double.MAX_VALUE))
            .withLeftSon(l6)
            .withRightSon(l7)
            .withIsXSplit(false)
            .withIsLeaf(false)
            .build();

    // leaf Parent Parent Parent Node (root)
    private static Node l1 = nb
            .withArea(new Area(-Double.MAX_VALUE, -Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE))
            .withLeftSon(l2)
            .withRightSon(l3)
            .withIsXSplit(true)
            .withIsLeaf(false)
            .build();


    public static Node getL8() {
        return l8;
    }
    public static Node getL9() {
        return l9;
    }
    public static Node getL10() {
        return l10;
    }
    public static Node getL11() {
        return l11;
    }
    public static Node getL12() {
        return l12;
    }
    public static Node getL13() {
        return l13;
    }
    public static Node getL14() {
        return l14;
    }
    public static Node getL15() {
        return l15;
    }
    public static Node getL4() {
        return l4;
    }
    public static Node getL5() {
        return l5;
    }
    public static Node getL6() {
        return l6;
    }
    public static Node getL7() {
        return l7;
    }
    public static Node getL2() {
        return l2;
    }
    public static Node getL3() {
        return l3;
    }
    public static Node getL1() {
        return l1;
    }

}
