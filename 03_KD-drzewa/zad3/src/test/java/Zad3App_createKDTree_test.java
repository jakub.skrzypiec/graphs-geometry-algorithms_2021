import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * build KD_tree tests - verify output KD-tree structure
 */
public class Zad3App_createKDTree_test {

    public static Node createKDTreeResult(String fileName){
        // init Zad3_app (includes create KD Tree)
        Zad3_Application zad3_app = Zad3_Application.initZad3App(fileName);

        // extract KD_tree and return in
        Node KD_Tree_root = zad3_app.node;

        return KD_Tree_root;
    }


    @Test
    public void createKDTree_test01() {
        // given String fileName
        String fileName = "file0.txt";

        // when invoking 'create KD_Tree' method
        Node KD_Tree_root = createKDTreeResult(fileName);

        // then verify KD_Tree structure
        Node expected_L1 = CreateKDTreeTest_NodeGenerator.getL1(); // root
        assertEquals(expected_L1, KD_Tree_root);
    }
    @Test
    public void createKDTree_test02() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L2 = CreateKDTreeTest_NodeGenerator.getL2(); // root's left son
        assertEquals(expected_L2, KD_Tree_root.getLeftSon());
    }
    @Test
    public void createKDTree_test03() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L3 = CreateKDTreeTest_NodeGenerator.getL3(); // root's right son
        assertEquals(expected_L3, KD_Tree_root.getRightSon());
    }

    @Test
    public void createKDTree_test04() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L4 = CreateKDTreeTest_NodeGenerator.getL4(); // root's left son's left son
        assertEquals(expected_L4, KD_Tree_root.getLeftSon().getLeftSon());
    }

    @Test
    public void createKDTree_test05() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L5 = CreateKDTreeTest_NodeGenerator.getL5(); // root's left son's right son
        assertEquals(expected_L5, KD_Tree_root.getLeftSon().getRightSon());
    }

    @Test
    public void createKDTree_test06() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L6 = CreateKDTreeTest_NodeGenerator.getL6(); // root's right son's left son
        assertEquals(expected_L6, KD_Tree_root.getRightSon().getLeftSon());
    }

    @Test
    public void createKDTree_test07() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L7 = CreateKDTreeTest_NodeGenerator.getL7(); // root's right son's right son
        assertEquals(expected_L7, KD_Tree_root.getRightSon().getRightSon());
    }

    @Test
    public void createKDTree_test08() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L8 = CreateKDTreeTest_NodeGenerator.getL8(); // root's left son's left son's left son
        assertEquals(expected_L8, KD_Tree_root.getLeftSon().getLeftSon().getLeftSon());
    }

    @Test
    public void createKDTree_test09() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L9 = CreateKDTreeTest_NodeGenerator.getL9(); // root's left son's left son's right son
        assertEquals(expected_L9, KD_Tree_root.getLeftSon().getLeftSon().getRightSon());
    }

    @Test
    public void createKDTree_test10() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L10 = CreateKDTreeTest_NodeGenerator.getL10(); // root's left son's right son's left son
        assertEquals(expected_L10, KD_Tree_root.getLeftSon().getRightSon().getLeftSon());
    }

    @Test
    public void createKDTree_test11() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L11 = CreateKDTreeTest_NodeGenerator.getL11(); // root's left son's right son's right son
        assertEquals(expected_L11, KD_Tree_root.getLeftSon().getRightSon().getRightSon());
    }

    @Test
    public void createKDTree_test12() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L12 = CreateKDTreeTest_NodeGenerator.getL12(); // root's right son's left son's left son
        assertEquals(expected_L12, KD_Tree_root.getRightSon().getLeftSon().getLeftSon());
    }

    @Test
    public void createKDTree_test13() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L13 = CreateKDTreeTest_NodeGenerator.getL13(); // root's right son's left son's right son
        assertEquals(expected_L13, KD_Tree_root.getRightSon().getLeftSon().getRightSon());
    }

    @Test
    public void createKDTree_test14() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L14 = CreateKDTreeTest_NodeGenerator.getL14(); // root's right son's right son's left son
        assertEquals(expected_L14, KD_Tree_root.getRightSon().getRightSon().getLeftSon());
    }

    @Test
    public void createKDTree_test15() {
        String fileName = "file0.txt";
        Node KD_Tree_root = createKDTreeResult(fileName);

        Node expected_L15 = CreateKDTreeTest_NodeGenerator.getL15(); // root's right son's right son's right son
        assertEquals(expected_L15, KD_Tree_root.getRightSon().getRightSon().getRightSon());
    }


}
