import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AreaUtils_isPointInArea_test {

    /**
     * Point is in the Area - expect True
     */
    @Test
    public void areaUtils_isPointInArea_expectTrue_test01() {
        // given Area and Point
        Point point = new Point(2,2);
        Area area = new Area(0,0, 4,4);

        // when invoking method...
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);

        // then
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test02() {
        Point point = new Point(0,0);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test03() {
        Point point = new Point(0,1);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test04() {
        Point point = new Point(0,2);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test05() {
        Point point = new Point(0,3);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test06() {
        Point point = new Point(0,4);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test07() {
        Point point = new Point(1,4);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test08() {
        Point point = new Point(2, 4);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test09() {
        Point point = new Point(3, 4);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test10() {
        Point point = new Point(4, 4);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test11() {
        Point point = new Point(4, 3);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test12() {
        Point point = new Point(4, 2);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test13() {
        Point point = new Point(4, 1);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test14() {
        Point point = new Point(4, 0);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test15() {
        Point point = new Point(3, 0);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test16() {
        Point point = new Point(2, 0);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test17() {
        Point point = new Point(1, 0);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test18() {
        Point point = new Point(3, 3);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test19() {
        Point point = new Point(1, 1);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test20() {
        Point point = new Point(1, 3);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test21() {
        Point point = new Point(3, 1);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectTrue_test22() {
        Point point = new Point(2, 3);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertTrue(isPointInArea);
    }




    /**
     * Point is not in the Area - expect False
     */
    @Test
    public void areaUtils_isPointInArea_expectFalse_test01() {
        Point point = new Point(-1,-1);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectFalse_test02() {
        Point point = new Point(2, -1);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectFalse_test03() {
        Point point = new Point(2, 5);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectFalse_test04() {
        Point point = new Point(-1, 2);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectFalse_test05() {
        Point point = new Point(5, 2);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectFalse_test06() {
        Point point = new Point(10, 10);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectFalse_test07() {
        Point point = new Point(3, -1);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }

    @Test
    public void areaUtils_isPointInArea_expectFalse_test08() {
        Point point = new Point(1, 6);
        Area area = new Area(0,0, 4,4);
        boolean isPointInArea = AreaUtils.isPointInArea(point, area);
        assertFalse(isPointInArea);
    }


}
