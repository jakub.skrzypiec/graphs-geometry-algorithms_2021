/**
 * Fluent builder to build Nodes
 */
public class NodeBuilder {

    private Point point = null;
    private Node leftSon = null;
    private Node rightSon = null;
    private boolean isXsplit = false;
    private boolean isLeaf = true;
    private double xSplit = 0.0;
    private double ySplit = 0.0;
    private Area area = null;


    public NodeBuilder() {}


    private void resetFields() {
        point = null;
        leftSon = null;
        rightSon = null;
        isXsplit = false;
        isLeaf = true;
        xSplit = 0.0;
        ySplit = 0.0;
        area = null;
    }


    public Node build(){
        Node node = new Node(point, leftSon, rightSon, isXsplit, isLeaf, xSplit, ySplit, area);
        resetFields();
        return node;
    }

    public NodeBuilder withPoint(Point point){
        this.point = point;
        return this;
    }
    public NodeBuilder withLeftSon(Node leftSon){
        this.leftSon = leftSon;
        return this;
    }
    public NodeBuilder withRightSon(Node rightSon){
        this.rightSon = rightSon;
        return this;
    }
    public NodeBuilder withIsXSplit(boolean isXsplit){
        this.isXsplit = isXsplit;
        return this;
    }
    public NodeBuilder withIsLeaf(boolean isLeaf){
        this.isLeaf = isLeaf;
        return this;
    }
    public NodeBuilder withArea(Area area){
        this.area = area;
        return this;
    }

}
