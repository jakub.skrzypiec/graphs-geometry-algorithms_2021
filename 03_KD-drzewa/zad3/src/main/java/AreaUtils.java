public class AreaUtils {

    public static boolean doAreasIntersect(Area area1, Area area2) {
        // when they do NOT intersect
        if(area1.getP2x() < area2.getP1x() || area1.getP1x() > area2.getP2x()
                || area1.getP1y() > area2.getP2y() || area1.getP2y() < area2.getP1y()){
            return false;
        }
        return true;
    }

    public static boolean isPointInArea(Point point, Area area) {
        // when point is NOT in the area
        if(point.getX() < area.getP1x() || point.getX() > area.getP2x()
                || point.getY() < area.getP1y() || point.getY() > area.getP2y()) {
            return false;
        }
        return true;
    }


}
