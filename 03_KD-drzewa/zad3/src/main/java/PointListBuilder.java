import java.util.ArrayList;
import java.util.List;

/**
 * Fluent builder to build lists of points
 */
public class PointListBuilder {

    private List<Point> pointList = new ArrayList<>();


    public PointListBuilder() {}


    public PointListBuilder add(double x, double y) {
        this.pointList.add(new Point(x, y));
        return this;
    }

    public List<Point> build() {
        List<Point> pointList = this.pointList;
        this.pointList = new ArrayList<>();
        return pointList;
    }


}
