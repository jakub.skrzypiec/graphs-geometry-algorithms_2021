import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    // given String fileName - file with points list (each point in new line, x/y separated by ',' - see e.g. "file0.txt")
    // return
    public static List<Point> fileToPointList(String fileName){
        List<Point> pointList = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            String[] lineArr;
            while ((line = br.readLine()) != null){
                lineArr = line.split(",");
                if (lineArr.length == 2){
                    pointList.add(new Point(Double.parseDouble(lineArr[0]), Double.parseDouble(lineArr[1])));
                } else {
                    throw new IOException("File format is incorrect - provide one point per one line!");
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }
        return pointList;
    }

}
