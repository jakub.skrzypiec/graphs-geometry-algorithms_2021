import java.util.Objects;

public class Node {

    private Point point;
    private Node leftSon;
    private Node rightSon;
    private boolean isXsplit;
    private boolean isLeaf;
    private double xSplit;
    private double ySplit;
    private Area area;


    public Node() {}
    public Node(Point point, Node leftSon, Node rightSon) {
        this.point = point;
        this.leftSon = leftSon;
        this.rightSon = rightSon;
    }
    public Node(Point point, Node leftSon, Node rightSon, boolean isXsplit, boolean isLeaf, double xSplit, double ySplit, Area area) {
        this.point = point;
        this.leftSon = leftSon;
        this.rightSon = rightSon;
        this.isXsplit = isXsplit;
        this.isLeaf = isLeaf;
        this.xSplit = xSplit;
        this.ySplit = ySplit;
        this.area = area;
    }


    public Point getPoint() {
        return point;
    }
    public void setPoint(Point point) {
        this.point = point;
    }
    public Node getLeftSon() {
        return leftSon;
    }
    public void setLeftSon(Node leftSon) {
        this.leftSon = leftSon;
    }
    public Node getRightSon() {
        return rightSon;
    }
    public void setRightSon(Node rightSon) {
        this.rightSon = rightSon;
    }
    public boolean isXsplit() {
        return isXsplit;
    }
    public void setIsXsplit(boolean xSplit){
        this.isXsplit = xSplit;
    }
    public double getxSplit() {
        return xSplit;
    }
    public void setxSplit(double xSplit) {
        this.xSplit = xSplit;
    }
    public double getySplit() {
        return ySplit;
    }
    public void setySplit(double ySplit) {
        this.ySplit = ySplit;
    }
    public Area getArea() {
        return area;
    }
    public void setArea(Area area) {
        this.area = area;
    }
    public boolean isLeaf() {
        return isLeaf;
    }
    public void setIsLeaf(boolean leaf) {
        isLeaf = leaf;
    }


    @Override
    public String toString() {
        return "Node{" +
                "point=" + point +
                ", area=" + area +
                ", leftSon=" + leftSon +
                ", rightSon=" + rightSon +
                ", isXsplit=" + isXsplit +
                ", isLeaf=" + isLeaf +
                ", xSplit=" + xSplit +
                ", ySplit=" + ySplit +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return isXsplit == node.isXsplit && isLeaf == node.isLeaf && Objects.equals(point, node.point)
                && Double.compare(node.xSplit, xSplit) == 0 && Double.compare(node.ySplit, ySplit) == 0
                && Objects.equals(leftSon, node.leftSon) && Objects.equals(rightSon, node.rightSon) && Objects.equals(area, node.area);
    }
    @Override
    public int hashCode() {
        return Objects.hash(point, leftSon, rightSon, isXsplit, isLeaf, xSplit, ySplit, area);
    }

}
