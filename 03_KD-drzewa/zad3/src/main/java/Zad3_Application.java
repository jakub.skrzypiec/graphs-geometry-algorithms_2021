import java.util.ArrayList;
import java.util.List;

public class Zad3_Application {
    public Zad3_Application(){}
    public Zad3_Application(List<Point> s, List<Point> sx, List<Point> sy, Node node) {
        this.s = s;
        this.sx = sx;
        this.sy = sy;
        this.node = node;
    }

    // point list (S)
    List<Point> s = new ArrayList<>();

    // copy of 'pointList' sorted by X
    List<Point> sx = new ArrayList<>();
    // copy of 'pointList' sorted by Y
    List<Point> sy = new ArrayList<>();

    // subsets of Sx
    List<Point> s1x = new ArrayList<>();
    List<Point> s2x = new ArrayList<>();
    // subsets of Sy
    List<Point> s1y = new ArrayList<>();
    List<Point> s2y = new ArrayList<>();

    Node node = new Node();



    public static void main(String[] args) {
        // init Zad3_app
        Zad3_Application zad3_app = Zad3_Application.initZad3App("file0.txt");

        // search Area
        Area searchArea = new Area(0,0, 4,4);

        // search result
        List<Point> searchResult = zad3_app.searchKD_Tree(zad3_app.node, searchArea);

        // print search results
        PointUtils.printPointList("search result: ", searchResult);
    }

    // initialize the Zad3_app object
    public static Zad3_Application initZad3App(String fileName) {
        Zad3_Application zad3_app = new Zad3_Application();
        zad3_app.node.setIsXsplit(true);    // 1st split by X axis
        zad3_app.node.setIsLeaf(false);     // root is not a leaf ;)
        // set area to ('-inf','inf')x('+inf','+inf')
        zad3_app.node.setArea(new Area(-Double.MAX_VALUE, -Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE));

        List<Point> s = initializeSFromFile(fileName);  // read points from file
        zad3_app.sortSets(s);    // sort Sx and Sy - once;

        PointUtils.printPointList("S", zad3_app.s);
        PointUtils.printPointList("Sx", zad3_app.sx);
        PointUtils.printPointList("Sy", zad3_app.sy);
        System.out.println();

        // set Node (in this case Root of the Kd tree) [arg: s-pointList, isXsplit]
        zad3_app.node = zad3_app.createKD_Tree(zad3_app.s, zad3_app.node.isXsplit());

        return zad3_app;
    }

    // create KD tree from given point list (s), firs split should be by X axis, isXsplit should be == true
    public Node createKD_Tree(List<Point> s, boolean isXSplit) {
        // return new Node when points.size==1
        if(s.size() == 1){
            // Node containing 1 point and w/o children
            Node node = new Node(s.get(0), null, null);
            node.setIsXsplit(isXSplit);
            node.setIsLeaf(true);
            node.setArea(new Area(  this.node.getArea().getP1x(), this.node.getArea().getP1y(),
                                    this.node.getArea().getP2x(), this.node.getArea().getP2y()));
            System.out.println(node);
            return node;
        }

        // split S (by X or Y)
        if (isXSplit) { // split S in 2 by X axis   & also set xSplit
            this.splitSByX();
        } else { // split S in 2 by Y axis   & also set ySplit
            this.splitSByY();
        }
        PointUtils.printPointList("S1x", this.s1x);
        PointUtils.printPointList("S2x", this.s2x);
        PointUtils.printPointList("S1y", this.s1y);
        PointUtils.printPointList("S2y", this.s2y);

        // init Node
        Node node = new Node();
        node.setIsLeaf(false);
        // init Area (parent area)
        node.setArea(new Area(  this.node.getArea().getP1x(), this.node.getArea().getP1y(),
                                this.node.getArea().getP2x(), this.node.getArea().getP2y()));
        // init child Node's
        Node nodeLeft = new Node();
        Node nodeRight = new Node();
        nodeLeft.setArea(new Area(  this.node.getArea().getP1x(), this.node.getArea().getP1y(),
                                    this.node.getArea().getP2x(), this.node.getArea().getP2y()));
        nodeRight.setArea(new Area( this.node.getArea().getP1x(), this.node.getArea().getP1y(),
                                    this.node.getArea().getP2x(), this.node.getArea().getP2y()));
        // change child nodes area points...
        if(isXSplit) {
            nodeLeft.getArea().setP2x(this.node.getxSplit());
            nodeRight.getArea().setP1x(this.node.getxSplit());
        } else {
            nodeLeft.getArea().setP2y(this.node.getySplit());
            nodeRight.getArea().setP1y(this.node.getySplit());
        }

        // recursion for s subsets
        Zad3_Application zad3_app_X = new Zad3_Application(s1x, s1x, s1y, nodeLeft);
        Node leftSubTree = zad3_app_X.createKD_Tree(s1x, !isXSplit);
        Zad3_Application zad3_app_Y = new Zad3_Application(s2x, s2x, s2y, nodeRight);
        Node rightSubTree = zad3_app_Y.createKD_Tree(s2x, !isXSplit);

        // return Node cointaining 0 points and 2 sons - left & right subtrees
        node.setLeftSon(leftSubTree);
        node.setRightSon(rightSubTree);
        node.setIsXsplit(isXSplit);
        System.out.println(node);
        return node;
    }

    // KD-tree search - returns points from S (KD tree) that are in R (searchArea) area
    public List<Point> searchKD_Tree(Node kdTree, Area searchArea) {
        List<Point> searchResult = new ArrayList<>();
        // when Node area and search area intersect - search for points
        // if Node area and search area DO NOT intersect - there is no point in searching in that Node and it's children (return empty list)
        if(AreaUtils.doAreasIntersect(kdTree.getArea(), searchArea)) {
            if (kdTree.isLeaf()) {
                if (kdTree.getPoint() != null && AreaUtils.isPointInArea(kdTree.getPoint(), searchArea)) {
                    searchResult.add(kdTree.getPoint());
                }
                return searchResult;
            }

            // recursive search in children
            List<Point> leftSubTreeSearchResult = searchKD_Tree(kdTree.getLeftSon(), searchArea);
            List<Point> rightSubTreeSearchResult = searchKD_Tree(kdTree.getRightSon(), searchArea);

            // join results
            searchResult.addAll(leftSubTreeSearchResult);
            searchResult.addAll(rightSubTreeSearchResult);
        }
        return searchResult;
    }

    // split S (point list) in two sublists by X axis
    public void splitSByX(){
        int separationPointIndex = this.sx.size() / 2;
        Point separationPoint = this.sx.get(separationPointIndex - 1);

        System.out.println("[X] Separation point: " + separationPoint);

        this.s1x.addAll(this.sx.subList(0, separationPointIndex));                  // s1x = first 'half' of the sx  -- [0,sepPtIdx)
        this.s2x.addAll(this.sx.subList(separationPointIndex, this.sx.size()));     // s2x = second 'half' of the sx -- [sepPtIdx, sx.size)
        // s1y,s2y
        for (int i = 0; i < this.sy.size(); i++) {
            // lower (by Y) and separation point to s1y, higher to s2y
            if (PointUtils.compareTwoPointsXOrder(this.sy.get(i), separationPoint) == 1) {
                this.s2y.add(this.sy.get(i));
            } else {
                this.s1y.add(this.sy.get(i));
            }
        }

        // set X split line
        this.node.setxSplit(separationPoint.getX());
    }
    // split S (point list) in two sublists by Y axis
    public void splitSByY(){
        int separationPointIndex = this.sy.size() / 2;
        Point separationPoint = this.sy.get(separationPointIndex - 1);

        System.out.println("[Y] Separation point: " + separationPoint);

        this.s1y.addAll(this.sy.subList(0, separationPointIndex));            // s1y = first 'half' of the sy  -- [0,sepPtIdx)
        this.s2y.addAll(this.sy.subList(separationPointIndex, this.sy.size()));    // s2y = second 'half' of the sy -- [sepPtIdx, sx.size)
        // s1x,s2x
        for (int i = 0; i < this.sx.size(); i++) {
            // lower (by X) and separation point to s1x, higher to s2x
            if (PointUtils.compareTwoPointsYOrder(this.sx.get(i), separationPoint) == 1) {
                this.s2x.add(this.sx.get(i));
            } else {
                this.s1x.add(this.sx.get(i));
            }
        }

        // set Y split line
        this.node.setySplit(separationPoint.getY());
    }


    public static List<Point> initializeSFromFile(String fileName) {
        List<Point> s = FileUtils.fileToPointList(fileName);     // read point list
        if(s.size() == 0){
            System.out.println("Error! Failed to load the points from file - check file");
            return null;
        }
        return s;
    }

    // sort S by X and by Y to subsets (Sx and Sy)
    public void sortSets(List<Point> pointList) {
        this.s = pointList;
        this.sx = PointUtils.sortPointListByX(new ArrayList(this.s));    // sx - sorted by X
        this.sy = PointUtils.sortPointListByY(new ArrayList(this.s));    // sy - sorted by Y
    }

}
