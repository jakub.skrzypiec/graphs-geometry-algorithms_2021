# zad03 
## KD-trees - construction & search [Divide-and-conquer algorithm]

### Input
    - R - Set of points, 
    - Search area (2 points - bottom left and top right)
### Output
    - Set of points from R that are inside Search Area (R n SearchArea)
### Example
### KD tree construction (given set of points [S])
![xxx](IMG/3_1.png "xxx")
![xxx](IMG/3_2.png "xxx")
![xxx](IMG/3_3.png "xxx")
![xxx](IMG/3_4.png "xxx")
![xxx](IMG/3_5.png "xxx")
![xxx](IMG/3_6.png "xxx")
![xxx](IMG/3_7.png "xxx")
![xxx](IMG/3_8.png "xxx")
### KD tree search (given search area [R])
![xxx](IMG/3_10.png "xxx")
