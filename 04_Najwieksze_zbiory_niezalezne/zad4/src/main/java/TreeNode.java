import java.util.ArrayList;
import java.util.List;

public class TreeNode {

    private Vertex value;
    private List<Vertex> children = new ArrayList<>();


    public TreeNode() {}


    public Vertex getValue() {
        return value;
    }
    public void setValue(Vertex value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "TreeNode{" +
                "value=" + value +
                ", children=" + children +
                '}';
    }
}
