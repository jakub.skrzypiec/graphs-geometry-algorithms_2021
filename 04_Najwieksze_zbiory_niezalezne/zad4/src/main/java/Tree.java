import java.util.ArrayList;
import java.util.List;

public class Tree {

    private List<Vertex> vertices = new ArrayList<>();
    private TreeNode root;


    public Tree() {}
    public TreeNode getRoot() {
        return root;
    }
    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }
    public void setVertices(List<Vertex> vertices) {
        this.vertices = vertices;
    }


    @Override
    public String toString() {
        return "Tree{" +
                "vertices=" + vertices +
                ", root=" + root +
                '}';
    }

}
