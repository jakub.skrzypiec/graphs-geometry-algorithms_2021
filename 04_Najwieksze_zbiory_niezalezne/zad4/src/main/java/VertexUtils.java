import java.util.List;

public class VertexUtils {

    // returns Vertex or null;
    public static Vertex findVertexInList(List<Vertex> vertices, int v){
        for(Vertex vertex : vertices){
            if(vertex.getV() == v){
                return vertex;
            }
        }
        return null;
    }

}
