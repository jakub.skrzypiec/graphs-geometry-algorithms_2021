import org.apache.commons.lang3.StringUtils;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public static List<Vertex> fileToVertices(String fileName){
        List<Vertex> vertices = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            String[] lineArr;
            // 1st line - read vertices;
            if((line = br.readLine()) == null) {
                throw new IOException("File format is incorrect!");
            }
            lineArr = line.split(",");
            if (lineArr.length < 1) {
                throw new IOException("File format is incorrect!");
            }
            // create new vertices and add them
            for(String vertex : lineArr){
                if(StringUtils.isNumeric(vertex)){
                    vertices.add(new Vertex(Integer.parseInt(vertex)));
                }
            }
            while ((line = br.readLine()) != null){
                lineArr = line.split(":");
                if (lineArr.length != 2) {
                    throw new IOException("File format is incorrect!");
                }
                String vertex = lineArr[0];
                Vertex v = null;
                if(StringUtils.isNumeric(vertex)){
                    v = VertexUtils.findVertexInList(vertices, Integer.parseInt(vertex));
                }
                String edges = lineArr[1];
                String[] edgesArr = edges.split(",");
                if (edgesArr.length >= 1){
                    for(String edge : edgesArr){
                        if(StringUtils.isNumeric(edge)){
                            Vertex ve = VertexUtils.findVertexInList(vertices, Integer.parseInt(edge));
                            if(v != null){
                                v.addE(ve);
                            }
                        }
                    }
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }
        return vertices;
    }


}
