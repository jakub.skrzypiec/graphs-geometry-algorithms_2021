import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Vertex {

    private int v;
    private List<Vertex> e = new ArrayList<>();
    private List<Vertex> children = new ArrayList<>();
    private List<Vertex> grandChildren = new ArrayList<>();
    boolean isUsed = false;     // whether Vertex was used to construct a tree with root (one-time change)
    boolean isLeaf = false;     // whether Vertex is a leaf in a tree structure
    int vv;
    List<Integer> largestIndependentSet = new ArrayList<>();


    public Vertex(int v) {
        this.v = v;
    }


    public int getV() {
        return v;
    }
    public List<Vertex> getE() {
        return e;
    }
    public List<Vertex> getChildren() {
        return children;
    }
    public List<Vertex> getGrandChildren() {
        return grandChildren;
    }

    public void addE(Vertex e){
        this.e.add(e);
    }
    public void addChild(Vertex e){
        this.children.add(e);
    }
    public void addGrandchildren(List<Vertex> grandChildren){
        this.grandChildren.addAll(grandChildren);
    }
    public boolean hasGrandChildren(){
        if(this.grandChildren.size() > 0){
            return true;
        } else {
            return false;
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{v=" + v);
        // e part
        if(e.size() > 0){
            sb.append(", e=");
            for(int i=0; i<e.size(); i++){
                if(i == (e.size()-1)){
                    sb.append(e.get(i).getV());
                } else {
                    sb.append(e.get(i).getV() + ",");
                }
            }
        }
        // children part
        if(children.size()>0){
            sb.append(", children=");
            for(int i=0; i<children.size(); i++){
                if(i == (children.size()-1)){
                    sb.append(children.get(i).getV());
                } else {
                    sb.append(children.get(i).getV() + ",");
                }
            }
        }
        // grandchildren part
        if(grandChildren.size()>0){
            sb.append(", grandChildren=");
            for(int i=0; i<grandChildren.size(); i++){
                if(i == (grandChildren.size()-1)){
                    sb.append(grandChildren.get(i).getV());
                } else {
                    sb.append(grandChildren.get(i).getV() + ",");
                }
            }
        }
        sb.append(", isUsed:"+isUsed);
        sb.append(", isLeaf:"+isLeaf);
        if (vv > 0) {
            sb.append(", vv:" + vv);
        }
        sb.append("}");
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return v == vertex.v && Objects.equals(e, vertex.e);
    }
    @Override
    public int hashCode() {
        return Objects.hash(v, e);
    }


}
