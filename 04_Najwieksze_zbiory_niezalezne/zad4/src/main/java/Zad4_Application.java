public class Zad4_Application {

    public static void main(String[] args) {
        Tree tree = new Tree();
        // read vertices from file
        tree.setVertices(FileUtils.fileToVertices("file4.txt"));
        System.out.println("Tree before root selection" + tree);

        // select a root
        TreeNode root = TreeUtils.selectRoot(tree);
        root.setValue(tree.getVertices().get(0)); // mock selection
        System.out.println("Selected root: " + root);
        System.out.println("Tree after root selection" + tree);

        // create a tree structure
        TreeUtils.createTree(tree);
        System.out.println("Tree after structure change:" + tree);

        // calculate Largest independent set SIZE
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());
        System.out.println("Largest independent set SIZE: " + tree.getRoot().getValue().vv);
        System.out.println("Largest independent set: " + tree.getRoot().getValue().largestIndependentSet);
    }


}
