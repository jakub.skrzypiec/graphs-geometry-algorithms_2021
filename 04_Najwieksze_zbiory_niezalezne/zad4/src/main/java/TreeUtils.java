import java.util.Random;
import java.util.stream.Collectors;

public class TreeUtils {

    // method that randomly selects root for a given Tree
    public static TreeNode selectRoot(Tree tree){
        // select the root
        int treeSize = tree.getVertices().size();
        Random random = new Random();
        int rootIndex = random.ints(0, treeSize)
                .findFirst()
                .getAsInt();
        TreeNode root = new TreeNode();
        root.setValue(tree.getVertices().get(rootIndex));

        // set root in the tree
        tree.setRoot(root);

        // return root
        return root;
    }

    // helper method to find children of a given V
    public static void findChildren(Vertex v){
        for(int i=0; i<v.getE().size(); i++){
            Vertex vertex = v.getE().get(i);
            if(!vertex.isUsed) {
                v.addChild(vertex);
                vertex.isUsed = true;
            }
        }
        if(v.getChildren().size() > 0){
            for(int i=0; i<v.getChildren().size(); i++){
                Vertex child = v.getChildren().get(i);
                if(child.getChildren().size() == 0){
                    findChildren(child);
                }
            }
        }
    }
    // method to find grandchildren of a given V
    public static void findGrandchildren(Tree tree){
        // find for root
        Vertex root = tree.getRoot().getValue();
        for(int i=0; i<root.getChildren().size(); i++) {
            Vertex childChild = root.getChildren().get(i);
            if(childChild.getChildren().size() != 0){
                root.addGrandchildren(childChild.getChildren());
            }
        }
        for(int k=0; k<tree.getVertices().size(); k++){
            Vertex v = tree.getVertices().get(k);
            if(v.equals(tree.getRoot().getValue())){
                continue;
            }
            for(int i=0; i<v.getChildren().size(); i++) {
                Vertex child = v.getChildren().get(i);
                if(child.getChildren().size() != 0){
                    v.addGrandchildren(child.getChildren());
                }
            }
        }
    }

    /**
     * create Tree
     */
    // method that constructs a tree structure (tree should have selected root already)
    public static void createTree(Tree tree) {
        // set root's children
        tree.getRoot().getValue().isUsed = true;
        findChildren(tree.getRoot().getValue());
        // set other nodes' children
        for(int i=0; i<tree.getRoot().getValue().getChildren().size(); i++) {
            findChildren(tree.getRoot().getValue().getChildren().get(i));
        }
//        for(int i=0; i<tree.getVertices().size(); i++){
//            findChildren(tree.getVertices().get(i));
//        }
        // find grandchildren
        findGrandchildren(tree);
        // mark leaves
        for(int i=0; i<tree.getVertices().size(); i++){
            if(tree.getVertices().get(i).getChildren() == null || tree.getVertices().get(i).getChildren().size() == 0) {
                tree.getVertices().get(i).isLeaf = true;
                // tree.getVertices().get(i).vv = 1;
            }
        }

    }


    /**
     * calculate Largest independent set SIZE
     */
    // helper methods
    // calculateChildrenSumVV - calculates all v's children VV sum;
    public static int calculateChildrenSumVV(Vertex v) {
        int sumVV = 0;
        if(v.getChildren().size() > 0){
            for(Vertex child : v.getChildren()){
                sumVV += child.vv;
            }
        }
        return sumVV;
    }
    // calculateGrandchildrenSumVV - calculates all v's grandchildren VV sum;
    public static int calculateGrandchildrenSumVV(Vertex v) {
        int sumVV = 0;
        if(v.hasGrandChildren()){
            for(Vertex grandChild : v.getGrandChildren()){
                sumVV += grandChild.vv;
            }
        }
        return sumVV;
    }


    // method to calculate Largest independent set SIZE
    public static int calculateLargestIndependentSetSize(Vertex vertex) {
        if(vertex.isLeaf) {
            // leaf has vv = 1
            vertex.largestIndependentSet.add(vertex.getV());    // add vertex to largestIndSet
            vertex.vv = 1;
            return 1;
        } else if (vertex.getChildren().size()>0 && !vertex.hasGrandChildren()){
            // vertex has CHILDREN but 0 GRANDCHILDREN - vv is sum of children vv's
            // before summing - if any child has not yet calculated it's vv - calculate it...
            for(Vertex child : vertex.getChildren()){
                if(child.vv == 0){
                    calculateLargestIndependentSetSize(child); // RECURSION
                }
            }
            // add vertices (all children) to largestIndSet
            vertex.largestIndependentSet.addAll(
                    vertex.getChildren().stream().map(n -> n.getV()).collect(Collectors.toList())
            );
            int childrenVV = calculateChildrenSumVV(vertex);
            vertex.vv = childrenVV;
            return childrenVV;
        } else {
            // vertex has GRANDCHILDREN - vv is max(1+sum of grandchildren vv's; sum of children vv's)
            // before summing - if any grandchild/child has not yet calculated it's vv - calculate it...
            for(Vertex grandchild : vertex.getGrandChildren()){
                if(grandchild.vv == 0){
                    calculateLargestIndependentSetSize(grandchild); // RECURSION
                }
            }
            for(Vertex child : vertex.getChildren()){
                if(child.vv == 0){
                    calculateLargestIndependentSetSize(child); // RECURSION
                }
            }
            int grandchildrenAndCurrentVV = calculateGrandchildrenSumVV(vertex) + 1;    // 1+sum of grandchildren vv's
            int childrenVV = calculateChildrenSumVV(vertex);
            if(childrenVV > grandchildrenAndCurrentVV ||
                    childrenVV >= grandchildrenAndCurrentVV && vertex.getChildren().size() > (vertex.getGrandChildren().size()+1)){
                // add vertices (all children) to largestIndSet
                for(Vertex child : vertex.getChildren()){
                    vertex.largestIndependentSet.addAll(
                            child.largestIndependentSet
                    );
                }
                vertex.vv = childrenVV;
                return childrenVV;
            } else {
                // add vertices (all grandchildren + current) to largestIndSet
                for(Vertex grandChild : vertex.getGrandChildren()){
                    vertex.largestIndependentSet.addAll(
                        grandChild.largestIndependentSet
                    );
                }
                vertex.largestIndependentSet.add(vertex.getV());
                vertex.vv = grandchildrenAndCurrentVV;
                return grandchildrenAndCurrentVV;
            }
        }
    }


}
