import org.junit.Test;
import java.util.List;
import static org.junit.Assert.*;

public class VertexUtilsTests {

    /**
     * test where we expect "positive results" - the Vertex is in the list and should be returned
     */
    @Test
    public void findVertexInList_expectPositiveResult_test01(){
        // Given List of Vertices
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");

        // When executing VertexUtils.findVertexInList(List<Vertex> vertices, int v)
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 1);

        // Then verify results
        assertNotEquals(null, resultVertex);   // is it really not null
        assertEquals(1, resultVertex.getV());    // does it have proper 'v'
        assertEquals(3, resultVertex.getE().size());  // does it have proper size of neighbors (E)
    }
    @Test
    public void findVertexInList_expectPositiveResult_test02(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");

        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 2);

        assertNotEquals(null, resultVertex);
        assertEquals(2, resultVertex.getV());
        assertEquals(1, resultVertex.getE().size());
    }
    @Test
    public void findVertexInList_expectPositiveResult_test03(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 3);
        assertTrue(resultVertex != null);
        assertEquals(3, resultVertex.getV());
        assertEquals(1, resultVertex.getE().size());
    }
    @Test
    public void findVertexInList_expectPositiveResult_test04(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 4);
        assertNotEquals(null, resultVertex);
        assertEquals(4, resultVertex.getV());
        assertEquals(2, resultVertex.getE().size());
    }
    @Test
    public void findVertexInList_expectPositiveResult_test05(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 5);
        assertNotEquals(null, resultVertex);
        assertEquals(5, resultVertex.getV());
        assertEquals(1, resultVertex.getE().size());
    }

    /**
     * test where we expect "negative results" - the Vertex is NOT in the list and 'null' should be returned instead
     */
    @Test
    public void findVertexInList_expectNegativeResult_test01(){
        // Given List of Vertices
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");

        // When executing VertexUtils.findVertexInList(List<Vertex> vertices, int v)
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 6);    // 6 - not existing V - method should return null

        // Then verify results
        assertEquals(null, resultVertex);   // is it really null
    }
    @Test
    public void findVertexInList_expectNegativeResult_test02(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");

        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 0);    // 0 - not existing V - method should return null

        assertEquals(null, resultVertex);    // is it really null
    }
    @Test
    public void findVertexInList_expectNegativeResult_test03(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 10);    // 10 - not existing V - method should return null
        assertEquals(null, resultVertex);    // is it really null
    }
    @Test
    public void findVertexInList_expectNegativeResult_test04(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, 100);    // 100 - not existing V - method should return null
        assertEquals(null, resultVertex);    // is it really null
    }
    @Test
    public void findVertexInList_expectNegativeResult_test05(){
        List<Vertex> vertices = FileUtils.fileToVertices("file0.txt");
        Vertex resultVertex = VertexUtils.findVertexInList(vertices, -2);    // '-2' - not existing V - method should return null
        assertEquals(null, resultVertex);    // is it really null
    }


}
