import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TreeUtilsTests_calcLargIndSetSize {

    private Tree tree;

    public void calculateLargestIndependentSetSize_setUp(String fileName, int rootIndex) {
        tree = new Tree();
        tree.setVertices(FileUtils.fileToVertices(fileName));
        TreeNode root = TreeUtils.selectRoot(tree);
        root.setValue(tree.getVertices().get(rootIndex)); // mock root selection
        TreeUtils.createTree(tree);
    }

    /**
     * File 0 - "file0.txt"
     */
    // check largestIndependentSet
    @Test
    public void calculateLargestIndependentSetSize_test00(){
        // Given Tree with structure (root, child/grandchild relations, etc)
        calculateLargestIndependentSetSize_setUp("file0.txt",0);    // [1] is a root

        // When executing TreeUtils.calculateLargestIndependentSetSize(...) [calculate Largest independent set SIZE]
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());

        // Then verify results
        // verify result size - should be: 3
        assertEquals(3, tree.getRoot().getValue().vv);
        assertEquals(3, tree.getRoot().getValue().largestIndependentSet.size());
        // verify result Nodes (v's)..., should be: 2,3,4
        assertEquals(2, tree.getRoot().getValue().largestIndependentSet.get(0).intValue());
        assertEquals(3, tree.getRoot().getValue().largestIndependentSet.get(1).intValue());
        assertEquals(5, tree.getRoot().getValue().largestIndependentSet.get(2).intValue());

        // print out
        System.out.println("Largest independent set size: " + tree.getRoot().getValue().vv);
        System.out.println("Largest independents set: " + tree.getRoot().getValue().largestIndependentSet);
    }
    // check VV values
    @Test
    public void calculateLargestIndependentSet_test00(){
        // Given Tree with structure (root, child/grandchild relations, etc)
        calculateLargestIndependentSetSize_setUp("file0.txt",0);    // [1] is a root

        // When executing TreeUtils.calculateLargestIndependentSetSize(...) [calculate Largest independent set SIZE]
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());

        // Then verify results - check VV values
        // should be:
        /*
                [1(root)]: 3
                /   |     \
            [2]:1  [3]:1   [4]:1
                            |
                           [5]:1
         */
        assertEquals(3, tree.getRoot().getValue().vv);  // root:3
        assertEquals(1, tree.getRoot().getValue().getChildren().get(0).vv); // 2.v = 1
        assertEquals(1, tree.getRoot().getValue().getChildren().get(1).vv); // 3.v = 1
        assertEquals(1, tree.getRoot().getValue().getChildren().get(2).vv); // 4.v = 1
        assertEquals(1, tree.getRoot().getValue().getChildren().get(2).getChildren().get(0).vv); // 5.v = 1
    }

    /**
     * File 1 - "file1.txt"
     */
    // check largestIndependentSet
    @Test
    public void calculateLargestIndependentSetSize_test01(){
        calculateLargestIndependentSetSize_setUp("file1.txt", 5);   // [6] is a root
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());
        // verify result size - should be: 3
        assertEquals(3, tree.getRoot().getValue().vv);
        assertEquals(3, tree.getRoot().getValue().largestIndependentSet.size());

        // verify result Nodes (v's)..., should be: 2,3,4
        assertEquals(5, tree.getRoot().getValue().largestIndependentSet.get(0).intValue());
        assertEquals(4, tree.getRoot().getValue().largestIndependentSet.get(1).intValue());
        assertEquals(6, tree.getRoot().getValue().largestIndependentSet.get(2).intValue());

        // print out
        System.out.println("Largest independent set size: " + tree.getRoot().getValue().vv);
        System.out.println("Largest independents set: " + tree.getRoot().getValue().largestIndependentSet);
    }

    // check VV values
    // Then verify results - check VV values
    @Test
    public void calculateLargestIndependentSet_test01(){
        calculateLargestIndependentSetSize_setUp("file1.txt",5);    // [6] is a root
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());
        // verify results - check VV values
        // should be:
        /*
              [6(root)]:3 --> [3]:3 --> [4]:2 --> [2]:2 --> [1]:1 --> [5(leaf)]:1
         */
        assertEquals(3, tree.getRoot().getValue().vv);  // root:3
        assertEquals(3, tree.getRoot().getValue().getChildren().get(0).vv); // 3.v = 3
        assertEquals(2, tree.getRoot().getValue().getChildren().get(0).getChildren().get(0).vv); // 4.v = 2
        assertEquals(2, tree.getRoot().getValue().getChildren().get(0).getChildren().get(0).getChildren().get(0).vv);  // 2.v = 2
        assertEquals(1, tree.getRoot().getValue().getChildren().get(0).getChildren().get(0).getChildren().get(0)
                                    .getChildren().get(0).vv); // 1.v = 1
        assertEquals(1, tree.getRoot().getValue().getChildren().get(0).getChildren().get(0).getChildren().get(0)
                                    .getChildren().get(0).getChildren().get(0).vv); // 5.v = 1
    }

    /**
     * File 2 - "file2.txt"
     */
    // check largestIndependentSet
    @Test
    public void calculateLargestIndependentSetSize_test02(){
        calculateLargestIndependentSetSize_setUp("file2.txt", 0);   // [7] is a root
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());
        // verify result size - should be: 5
        assertEquals(5, tree.getRoot().getValue().vv);
        assertEquals(5, tree.getRoot().getValue().largestIndependentSet.size());

        // verify result Nodes (v's)..., should be: 4,5,6,8,1
        assertEquals(8, tree.getRoot().getValue().largestIndependentSet.get(0).intValue());
        assertEquals(5, tree.getRoot().getValue().largestIndependentSet.get(1).intValue());
        assertEquals(4, tree.getRoot().getValue().largestIndependentSet.get(2).intValue());
        assertEquals(6, tree.getRoot().getValue().largestIndependentSet.get(3).intValue());
        assertEquals(1, tree.getRoot().getValue().largestIndependentSet.get(4).intValue());

        // print out
        System.out.println("Largest independent set size: " + tree.getRoot().getValue().vv);
        System.out.println("Largest independents set: " + tree.getRoot().getValue().largestIndependentSet);
    }

    // check VV values
    // Then verify results - check VV values
    @Test
    public void calculateLargestIndependentSet_test02(){
        calculateLargestIndependentSetSize_setUp("file2.txt",0);    // [7] is a root
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());
        // verify results - check VV values
        /*
                 [7(root)]: 5
                 /       \
               [8]:1    [1]:4
                       /      \
                    [3]:1     [2]:2
                    /        /     \
                  [5]:1   [4]:1   [6]:1

         */
        assertEquals(5, tree.getRoot().getValue().vv);  // [7(root)).v = 5
        assertEquals(1, tree.getRoot().getValue().getChildren().get(0).vv); // 8.v = 1
        assertEquals(4, tree.getRoot().getValue().getChildren().get(1).vv); // 1.v = 4
        assertEquals(1, tree.getRoot().getValue().getChildren().get(1).getChildren().get(0).vv); // 3.v = 1
        assertEquals(1, tree.getRoot().getValue().getChildren().get(1).getChildren().get(0).getChildren().get(0).vv);// 5.v = 1
        assertEquals(2, tree.getRoot().getValue().getChildren().get(1).getChildren().get(1).vv); // 2.v = 2
        assertEquals(1, tree.getRoot().getValue().getChildren().get(1).getChildren().get(1).getChildren().get(0).vv); // 4.v = 1
        assertEquals(1, tree.getRoot().getValue().getChildren().get(1).getChildren().get(1).getChildren().get(1).vv); // 6.v = 1
    }

    /**
     * File 3 - "file3.txt"
     */
    // check largestIndependentSet
    @Test
    public void calculateLargestIndependentSetSize_test03(){
        calculateLargestIndependentSetSize_setUp("file3.txt", 0);   // [1] is a root
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());
        // verify result size - should be: 5
        assertEquals(5, tree.getRoot().getValue().vv);
        assertEquals(5, tree.getRoot().getValue().largestIndependentSet.size());

        // verify result Nodes (v's)..., should be: 4,5,6,7,1
        assertEquals(4, tree.getRoot().getValue().largestIndependentSet.get(0).intValue());
        assertEquals(5, tree.getRoot().getValue().largestIndependentSet.get(1).intValue());
        assertEquals(6, tree.getRoot().getValue().largestIndependentSet.get(2).intValue());
        assertEquals(7, tree.getRoot().getValue().largestIndependentSet.get(3).intValue());
        assertEquals(1, tree.getRoot().getValue().largestIndependentSet.get(4).intValue());

        // print out
        System.out.println("Largest independent set size: " + tree.getRoot().getValue().vv);
        System.out.println("Largest independents set: " + tree.getRoot().getValue().largestIndependentSet);
    }

    /**
     * File 4 - "file4.txt"
     */
    // check largestIndependentSet
    @Test
    public void calculateLargestIndependentSetSize_test04(){
        calculateLargestIndependentSetSize_setUp("file4.txt", 0);   // [1] is a root
        tree.getRoot().getValue().vv = TreeUtils.calculateLargestIndependentSetSize(tree.getRoot().getValue());
        // verify result size - should be: 9
        assertEquals(9, tree.getRoot().getValue().vv);
        assertEquals(9, tree.getRoot().getValue().largestIndependentSet.size());

        // verify result Nodes (v's)..., should be: 13,4,5, 6,9,10, 11,12,1
        assertEquals(13, tree.getRoot().getValue().largestIndependentSet.get(0).intValue());
        assertEquals(4, tree.getRoot().getValue().largestIndependentSet.get(1).intValue());
        assertEquals(5, tree.getRoot().getValue().largestIndependentSet.get(2).intValue());
        assertEquals(6, tree.getRoot().getValue().largestIndependentSet.get(3).intValue());
        assertEquals(9, tree.getRoot().getValue().largestIndependentSet.get(4).intValue());
        assertEquals(10, tree.getRoot().getValue().largestIndependentSet.get(5).intValue());
        assertEquals(11, tree.getRoot().getValue().largestIndependentSet.get(6).intValue());
        assertEquals(12, tree.getRoot().getValue().largestIndependentSet.get(7).intValue());
        assertEquals(1, tree.getRoot().getValue().largestIndependentSet.get(8).intValue());

        // print out
        System.out.println("Largest independent set size: " + tree.getRoot().getValue().vv);
        System.out.println("Largest independents set: " + tree.getRoot().getValue().largestIndependentSet);
    }


}
