import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class FileUtilsTests {

    @Test
    public void fileToVertices_test01(){
        // Given fileName
        String fileName = "file0.txt";

        // When performing FileUtils.fileToVertices(String fileName)
        List<Vertex> vertices = FileUtils.fileToVertices(fileName);

        // Then verify results
        assertEquals(5, vertices.size());   // expect 5 vertices
        // check neighbor relations (E)
        // 1 -> 2,3,4
        assertEquals(3, vertices.get(0).getE().size()); // 3 neighbors
        assertEquals(2, vertices.get(0).getE().get(0).getV()); // 1 -> 2
        assertEquals(3, vertices.get(0).getE().get(1).getV()); // 1 -> 3
        assertEquals(4, vertices.get(0).getE().get(2).getV()); // 1 -> 4
        // 2 -> 1
        assertEquals(1, vertices.get(1).getE().size());
        assertEquals(1, vertices.get(1).getE().get(0).getV());
        // 3 -> 1
        assertEquals(1, vertices.get(2).getE().size());
        assertEquals(1, vertices.get(2).getE().get(0).getV());
        // 4 -> 1,5
        assertEquals(2, vertices.get(3).getE().size());
        assertEquals(1, vertices.get(3).getE().get(0).getV());
        assertEquals(5, vertices.get(3).getE().get(1).getV());
        // 5 -> 4
        assertEquals(1, vertices.get(4).getE().size());
        assertEquals(4, vertices.get(4).getE().get(0).getV());
    }

}
