import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TreeUtilsTests_createTree {

    private Tree tree;

    // before each test
    @Before
    public void selectRoot_setUp() {
        tree = new Tree();
        tree.setVertices(FileUtils.fileToVertices("file0.txt"));
        TreeNode root = TreeUtils.selectRoot(tree);
        root.setValue(tree.getVertices().get(0)); // mock root selection
    }

    @Test
    public void selectRoot_test01(){
        // Given a Tree with already selected root (here selection is simulated - root is simulated manually)
        // code executed in 'selectRoot_setUp()' method

        // When executing TreeUtils.createTree(Tree tree)
        TreeUtils.createTree(tree);

        // Then verify results
        assertEquals(1, tree.getRoot().getValue().getV());  // 1 is root
        assertEquals(3, tree.getRoot().getValue().getChildren().size());  // root (1) has 3 children
        assertEquals(1, tree.getRoot().getValue().getGrandChildren().size());  // root (1) has 1 grandchild
    }

    @Test
    public void selectRoot_test02(){
        TreeUtils.createTree(tree);
        // root's children adn grandchildren verification (chldren should be: 2,3,4, grandchild: 5)
        assertEquals(2, tree.getRoot().getValue().getChildren().get(0).getV());
        assertEquals(3, tree.getRoot().getValue().getChildren().get(1).getV());
        assertEquals(4, tree.getRoot().getValue().getChildren().get(2).getV());
        assertEquals(5, tree.getRoot().getValue().getGrandChildren().get(0).getV());

    }

    @Test
    public void selectRoot_test03(){
        TreeUtils.createTree(tree);
        // root's son (4) children verification (should be size:1, v:5)
        assertEquals(1, tree.getRoot().getValue().getChildren().get(2).getChildren().size());
        assertEquals(5, tree.getRoot().getValue().getChildren().get(2).getChildren().get(0).getV());
    }

    @Test
    public void selectRoot_test04(){
        TreeUtils.createTree(tree);
        // verify leaves
        assertEquals(true, tree.getRoot().getValue().getChildren().get(0).isLeaf);
        assertEquals(true, tree.getRoot().getValue().getChildren().get(1).isLeaf);
        assertEquals(false, tree.getRoot().getValue().getChildren().get(2).isLeaf); // 4 has son: 5
        assertEquals(true, tree.getRoot().getValue().getChildren().get(2).getChildren().get(0).isLeaf);
    }



}
