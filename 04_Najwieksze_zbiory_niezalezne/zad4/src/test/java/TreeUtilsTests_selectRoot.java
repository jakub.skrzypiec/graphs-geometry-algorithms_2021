import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TreeUtilsTests_selectRoot {

    @Test
    public void selectRoot_test01(){
        // Given a Tree with just Vertices (no structure yet - no root, no child relations, etc)
        Tree tree = new Tree();
        tree.setVertices(FileUtils.fileToVertices("file0.txt"));    // read vertices from file

        // When NOT executing TreeUtils.selectRoot(Tree tree)
        // do nothing :) //

        // Then verify results - whether Tree.root is null
        assertEquals(null, tree.getRoot());
    }

    @Test
    public void selectRoot_test02(){
        // Given a Tree with just Vertices (no structure yet - no root, no child relations, etc)
        Tree tree = new Tree();
        tree.setVertices(FileUtils.fileToVertices("file0.txt"));    // read vertices from file

        // When executing TreeUtils.selectRoot(Tree tree)
        TreeNode root = TreeUtils.selectRoot(tree);

        // Then verify results - whether Tree.root is not null
        assertNotEquals(null, tree.getRoot());  // check the tree.root
        assertNotEquals(null, root);    // check the method output
    }

}
