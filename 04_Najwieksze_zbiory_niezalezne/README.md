# zad04
## Largest independent set (in subtree with root - r) [Dynamic programming]
### Find the largest set of graph (tree) vertices, so that no two nodes from that set will be neighbors

### Input
    - graph (tree) w/o root
### Output
    - integer: largest independent set size,
    - set: largest independetnt set

### Example:
![xxx](IMG/4_1.png "xxx")

### Select root (randomly from all the Tree nodes)
![xxx](IMG/4_2.png "xxx")

### Create Tree structure (root, child/grandchild relations, etc.)
![xxx](IMG/4_3.png "xxx")

### Assign leaves with *vv*=1
![xxx](IMG/4_4.png "xxx")

### Assign *vv* values to remaining tree nodes - Going from leaves to root
In each node (node with grandchildren) we will calculate 2 values:  
- childrenVVsum -- sum of node's all childrens' vv values   
- grandchildrenAndCurrentVVsum -- sum of all node's grandchildren vv values + 1 (current node)

Then we select bigger value of the 2 values.  
The largest independent set of the whole tree will be VV value of root.  
![xxx](IMG/4_5.png "xxx")

### Read the result (Largest independent set in Tree)
![xxx](IMG/4_6.png "xxx")