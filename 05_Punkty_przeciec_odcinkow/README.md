# zad05
## Intersection points of line segments on the Euclidean plane [Sweep line algorithm]
### Find all intersection points of given line segments (for simplicity only vertical and horizontal line segments are allowed)

### Input 
    - Set of horizontal and vertical line segments  
    e.g. [(1,1; 4,1), (2,0; 2,2), (3,0; 3,2)]
### Output 
    - Set of interesction points for given line segments  
    e.g. [(2,1), (3,1)]

### Example:
### Read all line segments from file
![xxx](IMG/5_1.png "xxx")  
### Check which line segments are vertical/horizontal
![xxx](IMG/5_2.png "xxx")  
### Create S_element list 
List of elements that contain:  
- x value,
- line segment (either horizontal or vertical and reference to it),
- type (enum: LeftEndOfHorizontal, RightEndOfHorizontal, Vertical)
![xxx](IMG/5_3.png "xxx")  
### Sort S_element list 
Sort by:
- x ASC, type (leftEndHor < vertical < rightEndHor) ASC, (yMin ASC, yMax ASC -- if needed for horizontal line semgnets)
![xxx](IMG/5_4.png "xxx")  
### Run the alg
Initialize empty sweep status = [],  
Iterate over S_element list and for each element perform action depending on that element type:  
- if it is a leftEndOfHorizontal --> add that horizontal line segment to sweep status,
- if it is a rightEndOfHorizontal --> remove that horizontal line segment from sweep status,
- if it is a vertical --> check intersections between that one vertical line segments and all horizontal line segments from sweep status, if there are any intersection, add them to the final alg output (intersection points list).  
![xxx](IMG/5_5.png "xxx")  
![xxx](IMG/5_6.png "xxx")  
![xxx](IMG/5_7.png "xxx")  
![xxx](IMG/5_8.png "xxx")  
![xxx](IMG/5_9.png "xxx")  
![xxx](IMG/5_10.png "xxx")  
![xxx](IMG/5_11.png "xxx")  
