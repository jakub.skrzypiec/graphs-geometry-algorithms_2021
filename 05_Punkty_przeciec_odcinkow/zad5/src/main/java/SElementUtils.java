import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SElementUtils {

    private static Logger logger = Logger.getLogger(SElementUtils.class);


    // creates list of Selement (and sorts this list)
    public static List<SElement> lineSegmentLisToSElementList(List<LineSegment> lineSegmentList){
        List<SElement> sElementList = new ArrayList<>();

        logger.info("CREATING S_ELEMENT LIST FROM LINESEGMENT LIST...");
        for(LineSegment lineSegment : lineSegmentList){
            if (lineSegment instanceof HorizontalLineSegment){
                HorizontalLineSegment horizontalLineSegment = (HorizontalLineSegment) lineSegment;
                // add both horizontal line segment ends' x's as a SElements to sElementList
                SElement sElement1 = new SElement(horizontalLineSegment.getxMin(), horizontalLineSegment, SElement.Type.LEFT_END_OF_HORIZONTAL);
                SElement sElement2 = new SElement(horizontalLineSegment.getxMax(), horizontalLineSegment, SElement.Type.RIGHT_END_OF_HORIZONTAL);
                sElementList.add(sElement1);
                sElementList.add(sElement2);
                logger.debug("SElement (horizontalLeftEnd): " + sElement1);
                logger.debug("SElement (horizontalRightEnd): " + sElement2);
            } else if (lineSegment instanceof VerticalLineSegment) {
                VerticalLineSegment verticalLineSegment = (VerticalLineSegment) lineSegment;
                // add vertical line segment x as a S_element to s_elements(list)
                SElement sElement = new SElement(verticalLineSegment.getX(), verticalLineSegment, SElement.Type.VERTICAL);
                sElementList.add(sElement);
                logger.debug("SElement (vertical): " + sElement);
            }
        }
        logger.info("CREATED S_ELEMENT LIST:");
        logger.info(sElementList);

        logger.info("SORTING S_ELEMENT LIST...");
        sElementList = sortSElementList(sElementList);
        logger.info("SORTED S_ELEMENT LIST:");
        logger.info(sElementList);

        for(SElement e : sElementList){
            logger.debug("\t" + e.getX() + " -- " + e);
        }

        return sElementList;
    }

    // helper method to sort SElement list - sort by X ASC, status ASC (LEFT, VERTICAL, RIGHT)
    public static List<SElement> sortSElementList(List<SElement> sElementList) {
        List<SElement> sortedSElementList = new ArrayList<>(sElementList);
        SElementsXAxisComparator sElementsXAxisComparator = new SElementsXAxisComparator();
        Collections.sort(sortedSElementList, sElementsXAxisComparator); // sort using SElementsXAxisComparator
        return sortedSElementList;
    }

}
