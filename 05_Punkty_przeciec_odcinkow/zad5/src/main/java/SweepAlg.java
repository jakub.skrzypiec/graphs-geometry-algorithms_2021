import org.apache.log4j.Logger;

import java.util.*;


public class SweepAlg {

    private static Logger logger = Logger.getLogger(SweepAlg.class);


    // GENERAL method to find intersections of line segments -- main method of the alg
    public static List<Point> findLineSegmentsIntersectionPoints(String fileName){
        // read lineSegmentList from file
        List<LineSegment> lineSegmentList = FileUtils.fileToLineSegmentsList(fileName);

        // create sElementList from lineSegmentList
        List<SElement> sElementList = SElementUtils.lineSegmentLisToSElementList(lineSegmentList);

        // create Sweep with empty status (status is a Tree of SElements [being horizontalLineSegments])
        Sweep sweep = new Sweep();

        // execute alg
        List<Point> intersectionPoints = SweepAlg.findLineSegmentsIntersectionPointsForGivenSElementListAndSweep(sElementList, sweep);
        logger.info("----------------------------------------------------------------------------------------------\n");
        return intersectionPoints;
    }


    // method to find intersection points (for given sElementList and Sweep)
    public static List<Point> findLineSegmentsIntersectionPointsForGivenSElementListAndSweep(List<SElement> sElementList, Sweep sweep) {
        List<Point> intersectionPointList = new ArrayList<>();

        logger.info("SEARCHING FOR INTERSECTION POINTS...");
        for(SElement sElement : sElementList){
            // if approaching leftEnd of horizontalLineSegment -- add that segment to Sweep status
            if (sElement.getType() == SElement.Type.LEFT_END_OF_HORIZONTAL) {
                sweep.statusAdd(sElement);
                logger.debug("sweepStatus.add: " + sElement);
                logger.debug("\tsweep status: " + sweep.getStatus());
            } // if approaching rightEnd of horizontalLineSegment -- remove that segment from Sweep status
            else if (sElement.getType() == SElement.Type.RIGHT_END_OF_HORIZONTAL) {
                if(sweep.statusContains(sElement)) {
                    sweep.statusRemove(sElement);
                    logger.debug("sweepStatus.remove: " + sElement);
                    logger.debug("\tsweep status: " + sweep.getStatus());
                }
            } // if approaching verticalLineSegment -- check potential intersecion points with Sweep status (horizontalLineSegments)
            else if (sElement.getType() == SElement.Type.VERTICAL && sweep.getStatus().size() > 0) {
                sweep.setCurrentVerticalLineSegment(sElement);
                logger.debug("sweepStatus.checkIntersections between " + sElement + " AND " + sweep.getStatus());
                intersectionPointList.addAll(sweepStatusAndVerticalLineSegmentIntersectionPoints(sweep));
            }
        }

        // intersectionPointList possibly contains dupicates (if 2 vertical points intersect 1 horizontal in the same point)
        // get rid of the duplicates (if duplicates will be needed - revert this change)
        intersectionPointList = new ArrayList<>(new HashSet<>(intersectionPointList));
        logger.info("INTERSECTION POINTS:");
        logger.info(intersectionPointList);
        return intersectionPointList;
    }

    // helper method to return intersection points of few horizontalLineSegments (sweep status) and 1 verticalLineSegment
    public static List<Point> sweepStatusAndVerticalLineSegmentIntersectionPoints(Sweep sweep) {
        List<Point> intersectionPoints = new ArrayList<>();
        VerticalLineSegment verticalLineSegment = (VerticalLineSegment) sweep.getCurrentVerticalLineSegment().getLineSegment();

        // get the subset of sweep.status (TreeSet) - to avoid checking each line segment in sweep status
        double y1 = verticalLineSegment.getyMin();
        double y2 = verticalLineSegment.getyMax();
        double x = verticalLineSegment.getX();
        VerticalLineSegment stubVertical1 = new VerticalLineSegment(y1, y1, x);
        VerticalLineSegment stubVertical2 = new VerticalLineSegment(y2, y2, x);
        SElement s1 = new SElement(x, stubVertical1, SElement.Type.VERTICAL);
        SElement s2 = new SElement(x, stubVertical2, SElement.Type.VERTICAL);
        TreeSet<SElement> subset = (TreeSet) sweep.getStatus().subSet(s1, true, s2, true);

        // for each SElement in correct yMin, yMax range - add intersection point
        for(SElement sElement : subset){
            if (sElement.getLineSegment() instanceof HorizontalLineSegment){
                HorizontalLineSegment horizontalLineSegment = (HorizontalLineSegment) sElement.getLineSegment();
                Point newIntersectionPoint = new Point(verticalLineSegment.getX(), horizontalLineSegment.getY());
                logger.debug("new intersection point! " + newIntersectionPoint);
                logger.debug("\tbetween: " + horizontalLineSegment + " AND " + verticalLineSegment);
                intersectionPoints.add(newIntersectionPoint);
            }
        }

        return intersectionPoints;
    }

}
