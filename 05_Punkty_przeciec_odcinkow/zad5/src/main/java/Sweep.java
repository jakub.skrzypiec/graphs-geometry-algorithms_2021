import org.apache.log4j.Logger;

import java.util.Set;
import java.util.TreeSet;

public class Sweep {

    private static Logger logger = Logger.getLogger(Sweep.class);


    private TreeSet<SElement> status = new TreeSet<>();  // tree, so operations (add, remove) cost less than in a list
    private SElement currentVerticalLineSegment;


    public Sweep() {
        logger.debug("new Sweep: " + this);
    }


    public TreeSet<SElement> getStatus() {
        return status;
    }
    public SElement getCurrentVerticalLineSegment() {
        return currentVerticalLineSegment;
    }
    public void setCurrentVerticalLineSegment(SElement currentVerticalLineSegment) {
        this.currentVerticalLineSegment = currentVerticalLineSegment;
    }

    public void statusAdd(SElement sElement) {
        this.status.add(sElement);
    }
    public void statusRemove(SElement sElement) {
        this.status.remove(sElement);
    }
    public boolean statusContains(SElement sElement){
        return this.status.contains(sElement);
    }


    @Override
    public String toString() {
        return "Sweep{" +
                ", status=" + status +
                ", currentVerticalLineSegment=" + currentVerticalLineSegment +
                '}';
    }

}
