import org.apache.commons.math3.util.Precision;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FileUtils {

    private static Logger logger = Logger.getLogger(FileUtils.class);


    public static List<LineSegment> fileToLineSegmentsList(String fileName){
        List<LineSegment> lineSegments = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            String[] lineArr;
            logger.info("READING LINE SEGMENTS FROM FILE...");
            while ((line = br.readLine()) != null){
                lineArr = line.split(";");
                if (lineArr.length != 2) {
                    throw new IOException("File format is incorrect - provide one line segment per line in format \"x1,y1;x2;y2\"");
                }
                // 2 points of the line segment:
                String[] point1;
                String[] point2;
                point1 = lineArr[0].split(",");
                point2 = lineArr[1].split(",");
                if (point1.length != 2 || point2.length != 2) {
                    throw new IOException("File format is incorrect - provide one line segment per line in format \"x1,y1;x2;y2\"");
                }
                // line segment points:
                double x1 = Double.parseDouble(point1[0]);
                double y1 = Double.parseDouble(point1[1]);
                double x2 = Double.parseDouble(point2[0]);
                double y2 = Double.parseDouble(point2[1]);
                // horizontal line segment:
                if(Precision.equals(y1, y2, 0.001d)) {
                    if (x1 < x2){
                        lineSegments.add(new HorizontalLineSegment(x1, x2, y1));
                    } else if (x1 > x2){
                        lineSegments.add(new HorizontalLineSegment(x2, x1, y1));
                    }
                    logger.debug(x1 + "," + y1 + "; " + x2 + "," + y2 + " - horizontal line segment");
                } // vertical line segment:
                else if (Precision.equals(x1, x2, 0.001d)) {
                    if (y1 < y2) {
                        lineSegments.add(new VerticalLineSegment(y1, y2, x1));
                    } else if (y1 > y2) {
                        lineSegments.add(new VerticalLineSegment(y2, y1, x1));
                    }
                    logger.debug(x1 + "," + y1 + "; " + x2 + "," + y2 + " - vertical line segment");
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }

        logger.info("CREATED LINE SEGMENT LIST:");
        logger.info(lineSegments);
        return lineSegments;
    }

}
