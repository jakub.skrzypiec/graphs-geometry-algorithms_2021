import org.apache.log4j.BasicConfigurator;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class Zad5_Application {

    public static void main(String[] args) {
        BasicConfigurator.configure();  // configure the logging (default)

        // List<Point> intersecionPointList =
//        SweepAlg.findLineSegmentsIntersectionPoints("file1.txt");


        HorizontalLineSegment h1 = new HorizontalLineSegment(4,6,11);
        HorizontalLineSegment h2 = new HorizontalLineSegment(3,5,10);
        HorizontalLineSegment h3 = new HorizontalLineSegment(5,7,7);
        SElement s1 = new SElement(4, h1, SElement.Type.LEFT_END_OF_HORIZONTAL);
        SElement s2 = new SElement(3, h2, SElement.Type.LEFT_END_OF_HORIZONTAL);
        SElement s3 = new SElement(5, h3, SElement.Type.LEFT_END_OF_HORIZONTAL);

        VerticalLineSegment v1 = new VerticalLineSegment(5,10,5);
        SElement s4 = new SElement(5, v1, SElement.Type.VERTICAL);

        ////
        Sweep sweep = new Sweep();
        sweep.setCurrentVerticalLineSegment(s4);
        sweep.statusAdd(s1);
        sweep.statusAdd(s2);
        sweep.statusAdd(s3);

        for(SElement sElement : sweep.getStatus()){
            System.out.println(sElement);
        }

        List<Point> interscetionPoints = SweepAlg.sweepStatusAndVerticalLineSegmentIntersectionPoints(sweep);
        System.out.println(interscetionPoints);


    }

}
