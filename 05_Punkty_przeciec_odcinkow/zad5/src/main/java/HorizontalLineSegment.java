public class HorizontalLineSegment extends LineSegment {

    private double xMin;
    private double xMax;
    private double y;


    public HorizontalLineSegment(double xMin, double xMax, double y) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.y = y;
    }


    public double getxMin() {
        return xMin;
    }
    public double getxMax() {
        return xMax;
    }
    public double getY() {
        return y;
    }


    @Override
    public String toString() {
        return "HorizontalLineSegment{" +
                "xMin=" + xMin +
                ", xMax=" + xMax +
                ", y=" + y +
                '}';
    }

}
