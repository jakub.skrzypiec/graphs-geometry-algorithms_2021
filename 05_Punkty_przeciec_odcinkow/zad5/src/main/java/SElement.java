public class SElement implements Comparable<SElement>{

    enum Type {
        LEFT_END_OF_HORIZONTAL, VERTICAL, RIGHT_END_OF_HORIZONTAL
    }
    private double x;
    private LineSegment lineSegment;
    private Type type;


    public SElement(double x, LineSegment lineSegment, Type type) {
        this.x = x;
        this.lineSegment = lineSegment;
        this.type = type;
    }


    public double getX() {
        return x;
    }
    public LineSegment getLineSegment() {
        return lineSegment;
    }
    public Type getType() {
        return type;
    }

    /**
     * returns
     * 1 - for LEFT_END_OF_HORIZONTAL
     * 2 - for VERTICAL
     * 3 - for RIGHT_END_OF_HORIZONTAL
     */
    public int getTypeValueNumeric() {
        switch(this.type){
            case LEFT_END_OF_HORIZONTAL: return 1;
            case VERTICAL: return 2;
            case RIGHT_END_OF_HORIZONTAL: return 3;
        }
        return -1;
    }

    /**
     * default SElement sorting (natural SElement order) -- used by TreeSet<SElement> to compare SElements (horizontal)
     *
     * compare only this.lineSegment and that.lineSegment because e.g. these 2 SElements are equal
     * (because they are both ends of the same horizontal line segment):
     *  - SElement (horizontalLeftEnd): S_element{x=3.0, lineSegment=HorizontalLineSegment{xMin=3.0, xMax=5.0, y=10.0}, type=LEFT_END_OF_HORIZONTAL}
     *  - SElement (horizontalLeftEnd): S_element{x=5.0, lineSegment=HorizontalLineSegment{xMin=3.0, xMax=5.0, y=10.0}, type=RIGHT_END_OF_HORIZONTAL}
     */
    @Override
    public int compareTo(SElement that) {
        if(this.getLineSegment() instanceof HorizontalLineSegment && that.getLineSegment() instanceof HorizontalLineSegment){
            HorizontalLineSegment thisHorLS = (HorizontalLineSegment) this.getLineSegment();
            HorizontalLineSegment thatHorLS = (HorizontalLineSegment) that.getLineSegment();
            if(thisHorLS.getY() < thatHorLS.getY()) {
                return -1;
            } else if (thisHorLS.getY() > thatHorLS.getY()){
                return 1;
            } else if(thisHorLS.getxMin() < thatHorLS.getxMin()) {
                return -1;
            } else if (thisHorLS.getxMin() > thatHorLS.getxMin()){
                return 1;
            } else if(thisHorLS.getxMax() < thatHorLS.getxMax()) {
                return -1;
            } else if (thisHorLS.getxMax() > thatHorLS.getxMax()){
                return 1;
            } else if (thisHorLS.getxMin() == thatHorLS.getxMin() && thisHorLS.getxMax() == thatHorLS.getxMax()
                    && thisHorLS.getY() == thatHorLS.getY()){
                return 0;
            }
            // when comparing Horizontal and Vertical - needed for TreeSet.subset...
        } else if(this.getLineSegment() instanceof HorizontalLineSegment && that.getLineSegment() instanceof VerticalLineSegment){
            HorizontalLineSegment thisHorLS = (HorizontalLineSegment) this.getLineSegment();
            VerticalLineSegment thatVerLS = (VerticalLineSegment) that.getLineSegment();
            if(thisHorLS.getY() < thatVerLS.getyMin() && thisHorLS.getY() < thatVerLS.getyMax()){
                return -1;
            } else if(thisHorLS.getY() > thatVerLS.getyMin() && thisHorLS.getY() > thatVerLS.getyMax()){
                return 1;
            }
        } else if(this.getLineSegment() instanceof VerticalLineSegment && that.getLineSegment() instanceof HorizontalLineSegment){
            VerticalLineSegment thisVerLS = (VerticalLineSegment) this.getLineSegment();
            HorizontalLineSegment thatHorLS = (HorizontalLineSegment) that.getLineSegment();
            if(thatHorLS.getY() < thisVerLS.getyMin() && thatHorLS.getY() < thisVerLS.getyMax()){
                return 1;
            } else if(thatHorLS.getY() > thisVerLS.getyMin() && thatHorLS.getY() > thisVerLS.getyMax()){
                return -1;
            }
        } else if(this.getLineSegment() instanceof VerticalLineSegment && that.getLineSegment() instanceof VerticalLineSegment){
            VerticalLineSegment thisVerLS = (VerticalLineSegment) this.getLineSegment();
            VerticalLineSegment thatVerLS = (VerticalLineSegment) that.getLineSegment();
            if(thisVerLS.getyMin() < thatVerLS.getyMin() && thisVerLS.getyMax() < thatVerLS.getyMax()) {
                return -1;
            } else if(thisVerLS.getyMin() > thatVerLS.getyMin() && thisVerLS.getyMax() > thatVerLS.getyMax()) {
                return 1;
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "S_element{" +
                "x=" + x +
                ", lineSegment=" + lineSegment +
                ", type=" + type +
                '}';
    }

}
