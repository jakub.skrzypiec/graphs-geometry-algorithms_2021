import java.util.Comparator;

/**
 * comparator to sort SElementList - sort by X ASC, TypeValueNumeric ASC, xMin ASC, xMax ASC
 */
public class SElementsXAxisComparator implements Comparator<SElement> {
    @Override
    public int compare(SElement o1, SElement o2) {
        if(o1.getX() < o2.getX()) {
            return -1;
        } else if (o1.getX() > o2.getX()) {
            return 1;
        } else {
            if (o1.getTypeValueNumeric() < o2.getTypeValueNumeric()) {
                return -1;
            } else if (o1.getTypeValueNumeric() > o2.getTypeValueNumeric()) {
                return 1;
            } else {
                if(o1.getLineSegment() instanceof HorizontalLineSegment && o2.getLineSegment() instanceof HorizontalLineSegment){
                    if(((HorizontalLineSegment) o1.getLineSegment()).getxMin() < ((HorizontalLineSegment) o2.getLineSegment()).getxMin()) {
                        return -1;
                    } else if (((HorizontalLineSegment) o1.getLineSegment()).getxMin() > ((HorizontalLineSegment) o2.getLineSegment()).getxMin()) {
                        return 1;
                    } else {
                        if(((HorizontalLineSegment) o1.getLineSegment()).getxMax() < ((HorizontalLineSegment) o2.getLineSegment()).getxMax()) {
                            return -1;
                        } else if (((HorizontalLineSegment) o1.getLineSegment()).getxMax() > ((HorizontalLineSegment) o2.getLineSegment()).getxMax()) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                } else {
                    return 0;
                }
            }
        }
    }

}
