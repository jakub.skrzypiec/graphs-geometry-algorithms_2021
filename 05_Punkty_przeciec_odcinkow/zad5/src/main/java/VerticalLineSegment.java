public class VerticalLineSegment extends LineSegment {

    private double yMin;
    private double yMax;
    private double x;


    public VerticalLineSegment(double yMin, double yMax, double x) {
        this.yMin = yMin;
        this.yMax = yMax;
        this.x = x;
    }


    public double getyMin() {
        return yMin;
    }
    public double getyMax() {
        return yMax;
    }
    public double getX() {
        return x;
    }


    @Override
    public String toString() {
        return "VerticalLineSegment{" +
                "yMin=" + yMin +
                ", yMax=" + yMax +
                ", x=" + x +
                '}';
    }

}
