import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SweepAlgTest {

    @BeforeClass
    public static void setup() {
        BasicConfigurator.configure();  // configure the logging (default)
    }


    /**
     * test the alg main output for given input (file)
     *
     * file 1 - "file1.txt"
     */
    @Test
    public void intersectionPointsList_test_01() {
        // given fine with horizontal and vertical points
        String fileName = "file1.txt";

        // when executing SweepAlg.findLineSegmentsIntersectionPoints(...) [aka find all intersection points for given line segments]
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints(fileName);

        // then verify result
        assertEquals("Incorrect intersectionPointList size!", 2, intersectionPointList.size());
        assertTrue(intersectionPointList.contains(new Point(2.0, 1.0)));
        assertTrue(intersectionPointList.contains(new Point(3.0, 1.0)));
    }

    /**
     * file 2 - "file2.txt"
     */
    @Test
    public void intersectionPointsList_test_02() {
        // given fine with horizontal and vertical points
        // when executing SweepAlg.findLineSegmentsIntersectionPoints(...)
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file2.txt");

        // then verify result
        assertEquals("Incorrect intersectionPointList size!", 2, intersectionPointList.size());
        assertTrue(intersectionPointList.contains(new Point(1.0, 1.0)));
        assertTrue(intersectionPointList.contains(new Point(1.0, 2.0)));
    }

    /**
     * file 3 - "file3.txt" - empty file - expect 0 intersections
     */
    @Test
    public void intersectionPointsList_test_03() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file3.txt");
        assertEquals("Incorrect intersectionPointList size!", 0, intersectionPointList.size());
    }

    /**
     * file 4 - "file4.txt" - only horizontal line segments - expect 0 intersections
     */
    @Test
    public void intersectionPointsList_test_04() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file4.txt");
        assertEquals("Incorrect intersectionPointList size!", 0, intersectionPointList.size());
    }

    /**
     * file 5 - "file5.txt" - only vertical line segments - expect 0 intersections
     */
    @Test
    public void intersectionPointsList_test_05() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file5.txt");
        assertEquals("Incorrect intersectionPointList size!", 0, intersectionPointList.size());
    }

    /**
     * file 6 - "file6.txt" - vertical & horizontal line segments, but 0 intersections - expect 0 intersections
     */
    @Test
    public void intersectionPointsList_test_06() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file6.txt");
        assertEquals("Incorrect intersectionPointList size!", 0, intersectionPointList.size());
    }

    /**
     * file 7 - "file7.txt"
     */
    @Test
    public void intersectionPointsList_test_07() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file7.txt");
        assertEquals("Incorrect intersectionPointList size!", 12, intersectionPointList.size());
        assertTrue(intersectionPointList.contains(new Point(3.0, 10.0)));
        assertTrue(intersectionPointList.contains(new Point(4.0, 10.0)));
        assertTrue(intersectionPointList.contains(new Point(5.0, 10.0)));
        assertTrue(intersectionPointList.contains(new Point(4.0, 11.0)));
        assertTrue(intersectionPointList.contains(new Point(5.0, 7.0)));
        assertTrue(intersectionPointList.contains(new Point(6.0, 7.0)));
        assertTrue(intersectionPointList.contains(new Point(6.0, 5.0)));
        assertTrue(intersectionPointList.contains(new Point(7.0, 5.0)));
        assertTrue(intersectionPointList.contains(new Point(7.0, 4.0)));
        assertTrue(intersectionPointList.contains(new Point(8.0, 4.0)));
        assertTrue(intersectionPointList.contains(new Point(8.0, 2.0)));
        assertTrue(intersectionPointList.contains(new Point(9.0, 2.0)));
    }

    /**
     * file 8 - "file8.txt"
     */
    @Test
    public void intersectionPointsList_test_08() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file8.txt");
        assertEquals("Incorrect intersectionPointList size!", 4, intersectionPointList.size());
        assertTrue(intersectionPointList.contains(new Point(0.0, 0.0)));
        assertTrue(intersectionPointList.contains(new Point(0.0, 1.0)));
        assertTrue(intersectionPointList.contains(new Point(1.0, 0.0)));
        assertTrue(intersectionPointList.contains(new Point(1.0, 1.0)));
    }

    /**
     * file 9 - "file9.txt"
     */
    @Test
    public void intersectionPointsList_test_09() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file9.txt");
        assertEquals("Incorrect intersectionPointList size!", 10, intersectionPointList.size());
        assertTrue(intersectionPointList.contains(new Point(0.0, 0.0)));
        assertTrue(intersectionPointList.contains(new Point(0.0, 1.0)));
        assertTrue(intersectionPointList.contains(new Point(0.0, 2.0)));
        assertTrue(intersectionPointList.contains(new Point(1.0, 2.0)));
        assertTrue(intersectionPointList.contains(new Point(1.0, 2.0)));
        assertTrue(intersectionPointList.contains(new Point(1.0, 2.0)));
        assertTrue(intersectionPointList.contains(new Point(3.0, 1.0)));
        assertTrue(intersectionPointList.contains(new Point(3.0, 2.0)));
        assertTrue(intersectionPointList.contains(new Point(4.0, 1.0)));
        assertTrue(intersectionPointList.contains(new Point(4.0, 2.0)));
    }

    /**
     * file 10- "file10.txt" - (15 horizontal, 18 vertical, 28 intersections) - expect 28 intersections
     */
    @Test
    public void intersectionPointsList_test_10() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file10.txt");
        assertEquals("Incorrect intersectionPointList size!", 28, intersectionPointList.size());
    }

    /**
     * file 11- "file11.txt" - expect 20 intersections
     */
    @Test
    public void intersectionPointsList_test_11() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file11.txt");
        assertEquals("Incorrect intersectionPointList size!", 20, intersectionPointList.size());
    }

    /**
     * file 12- "file12.txt" - only horizontal - expect 0 intersections
     */
    @Test
    public void intersectionPointsList_test_12() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file12.txt");
        assertEquals("Incorrect intersectionPointList size!", 0, intersectionPointList.size());
    }

    /**
     * file 13- "file13.txt" - (2 horizontal, 2 vertical, 4 intersections) - expect 4 intersections
     */
    @Test
    public void intersectionPointsList_test_13() {
        List<Point> intersectionPointList = SweepAlg.findLineSegmentsIntersectionPoints("file13.txt");
        assertEquals("Incorrect intersectionPointList size!", 4, intersectionPointList.size());
    }


}
