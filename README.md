# graphs-geometry-algorithms_2021
## Grafy, Geometria, Algorytmy - Jakub Skrzypiec (@jakub.skrzypiec) - 2021/22

Repozytorium zawiera zadania realizowane w ramach przedmiotu "Grafy, Geometria, Algorytmy"  
Informatyka II stopnia - Uniwersytet Gdański - 2021/22  
Opisy poszczególnych projektów/zadań znajdują się z podkatalogach.  

### EN
Graphs, geometry, algorithms  
Projects:  
- 1 - Polygon kernell (check whether polygon exists)  
- 2 - Shortest distance between 2 points (2d) [Divide-and-conquer algorithm]  
- 3 - KD-trees (2d, construction and search) 0 [Divide-and-conquer algorithm]  
- 4 - Largest independent set [Dynamic programming]  
- 5 - Intersection points of line segments on the Euclidean plane [Sweep line algorithm]  
- 6 - Nearest hospital problem [Approximation algorithm]  
- 7 - Minimal disjoint of the graph Problem [Randomized algorithm]  

Details about each project can be found in corresponding directory (en).  

## Build with
- Java

## Autor 
- Jakub Skrzypiec (@jakub.skrzypiec)
