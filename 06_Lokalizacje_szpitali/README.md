# zad06
## Nearest hospital problem [Approximation algorithm] 
### Given n-cities (N - set of cities) place k-hospitals so that the global max distance to hospital is minimalized (we minimalize largest distance to hospital)

### Input 
    - Set of Points: n-cities (size: n) 
    - Number: k-number of hospitals 
### Output 
    - Set of Points: k-cities (were the hospitals should be placed)

### Example:
Given n-points (n-cities) & k-number of cities, place k-cities among given n-cities.  
n=7 (7 cities), k=3  
![xxx](IMG/6_1.png "xxx")  
  
I) For 1-st hospital:  
1) Select 1-st city randomly and place hospiktal there  
![xxx](IMG/6_2.png "xxx")


2) Calculate distances between 1-st hospital and other cities (since it is one and only hospital it is nearest for all the remaining cities)  
![xxx](IMG/6_3.png "xxx")  


3) Find citiy which is the farthest from the 1-st hospital  
![xxx](IMG/6_4.png "xxx")  


II)  For 2..k hospitals:  
1) Place next hospital in the city which nearest hospital is the farthest distance (from all cities)  
![xxx](IMG/6_5.png "xxx")


2) Update distances - calculate distances between newly placed hospital and rest of the cities - if the new hospital is closer than old closest hospital, change closest hospital in that city  
![xxx](IMG/6_6.png "xxx")


3) Find citiy which is the farthest from the k-th hospital  
![xxx](IMG/6_7.png "xxx")  
  

...  
![xxx](IMG/6_8.png "xxx")  
![xxx](IMG/6_9.png "xxx")  
