import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class FileUtilsTest {

    @BeforeClass
    public static void setup() {
        BasicConfigurator.configure();  // configure the logging (default)
    }

    /**
     * test whether FileUtils read k (number) and CityList from a given files
     */
    /**
     * file 1 (file1.txt) - expect k=4, N=7 (7 cities)
     */
    @Test
    public void fileToKNumber_test01() {
        // given fileName
        String fileName = "file1.txt";

        // when executing FileUtils.fileToKNumber(String fileName)
        int k = FileUtils.fileToKNumber(fileName);

        // then verify results
        assertEquals(4, k);             // check k
    }
    @Test
    public void fileToCityList_test01() {
        // given fileName
        String fileName = "file1.txt";

        // when executing FileUtils.fileToKNumber(String fileName)
        List<City> cities = FileUtils.fileToCityList(fileName);

        // then verify results
        assertEquals(7, cities.size()); // check N (cities count)
        // verify cities locations...
        assertEquals(new Point(1,9), cities.get(0).getLocation());
        assertEquals(new Point(2,2), cities.get(1).getLocation());
        assertEquals(new Point(4,6), cities.get(2).getLocation());
        assertEquals(new Point(6,6), cities.get(3).getLocation());
        assertEquals(new Point(8,8), cities.get(4).getLocation());
        assertEquals(new Point(10,10), cities.get(5).getLocation());
        assertEquals(new Point(10,0), cities.get(6).getLocation());
    }


    /**
     * file 2 (file2.txt) - expect k=4, N=4 (4 cities)
     */
    @Test
    public void fileToKNumber_test02() {
        String fileName = "file2.txt";

        int k = FileUtils.fileToKNumber(fileName);

        assertEquals(4, k);
    }
    @Test
    public void fileToCityList_test02() {
        String fileName = "file2.txt";

        List<City> cities = FileUtils.fileToCityList(fileName);

        assertEquals(4, cities.size()); // check N (cities count)
        assertEquals(new Point(1,1), cities.get(0).getLocation());
        assertEquals(new Point(3,1), cities.get(1).getLocation());
        assertEquals(new Point(5,1), cities.get(2).getLocation());
        assertEquals(new Point(7,1), cities.get(3).getLocation());
    }


    /**
     * file 3 (file3.txt) - expect k=2, N=6 (6 cities)
     */
    @Test
    public void fileToKNumber_test03() {
        String fileName = "file3.txt";

        int k = FileUtils.fileToKNumber(fileName);

        assertEquals(2, k);
    }
    @Test
    public void fileToCityList_test03() {
        String fileName = "file3.txt";

        List<City> cities = FileUtils.fileToCityList(fileName);

        assertEquals(6, cities.size()); // check N (cities count)
        assertEquals(new Point(1,1), cities.get(0).getLocation());
        assertEquals(new Point(3,1), cities.get(1).getLocation());
        assertEquals(new Point(5,1), cities.get(2).getLocation());
        assertEquals(new Point(7,1), cities.get(3).getLocation());
        assertEquals(new Point(9,1), cities.get(4).getLocation());
        assertEquals(new Point(11,1), cities.get(5).getLocation());
    }


    /**
     * file 4 (file4.txt) - expect k=3, N=9 (9 cities)
     */
    @Test
    public void fileToKNumber_test04() {
        String fileName = "file4.txt";

        int k = FileUtils.fileToKNumber(fileName);

        assertEquals(3, k);
    }
    @Test
    public void fileToCityList_test04() {
        String fileName = "file4.txt";

        List<City> cities = FileUtils.fileToCityList(fileName);

        assertEquals(9, cities.size()); // check N (cities count)
        assertEquals(new Point(1,1), cities.get(0).getLocation());
        assertEquals(new Point(2,1), cities.get(1).getLocation());
        assertEquals(new Point(3,1), cities.get(2).getLocation());
        assertEquals(new Point(4,1), cities.get(3).getLocation());
        assertEquals(new Point(5,1), cities.get(4).getLocation());
        assertEquals(new Point(6,1), cities.get(5).getLocation());
        assertEquals(new Point(7,1), cities.get(6).getLocation());
        assertEquals(new Point(8,1), cities.get(7).getLocation());
        assertEquals(new Point(9,1), cities.get(8).getLocation());
    }

}
