import org.apache.commons.math3.util.Precision;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClosestHospitalAlgTest {

    @BeforeClass
    public static void setup() {
        BasicConfigurator.configure();  // configure the logging (default)
    }

    /**
     * file1 - file1.txt
     */
    @Test
    public void executeAlg_file01_test01() {
        // given fileName
        String fileName = "file1.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(1));

        // then verify results
        assertEquals(4, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 5.66);
        // check hospitals
        assertEquals(new Point(2,2), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(10,10), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(10,0), closestHospitalAlg.getHopistalList().get(2).getLocation());
        assertEquals(new Point(1,9), closestHospitalAlg.getHopistalList().get(3).getLocation());
    }


    /**
     * file2 - file2.txt
     */
    @Test
    public void executeAlg_file02_test01() {
        // given fileName
        String fileName = "file2.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(0));

        // then verify results
        assertEquals(4, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 0.01);
        // check hospitals
        assertEquals(new Point(1,1), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(7,1), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(3,1), closestHospitalAlg.getHopistalList().get(2).getLocation());
        assertEquals(new Point(5,1), closestHospitalAlg.getHopistalList().get(3).getLocation());
        // check whether each city distance is 0.00 (every city has its own hospital)
        assertTrue(Precision.equals(closestHospitalAlg.getCities().get(0).getClosestHospitalDistance(), 0.00, 0.01));
        assertTrue(Precision.equals(closestHospitalAlg.getCities().get(1).getClosestHospitalDistance(), 0.00, 0.01));
        assertTrue(Precision.equals(closestHospitalAlg.getCities().get(2).getClosestHospitalDistance(), 0.00, 0.01));
        assertTrue(Precision.equals(closestHospitalAlg.getCities().get(3).getClosestHospitalDistance(), 0.00, 0.01));
    }


    /**
     * file3 - file3.txt
     */
    @Test
    public void executeAlg_file03_test01() {
        // given fileName
        String fileName = "file3.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(0));

        // then verify results
        assertEquals(2, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 4.01);
        // check hospitals
        assertEquals(new Point(1,1), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(11,1), closestHospitalAlg.getHopistalList().get(1).getLocation());
    }
    @Test
    public void executeAlg_file03_test02() {
        // given fileName
        String fileName = "file3.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(1));

        // then verify results
        assertEquals(2, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 4.01);
        // check hospitals
        assertEquals(new Point(3,1), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(11,1), closestHospitalAlg.getHopistalList().get(1).getLocation());
    }
    @Test
    public void executeAlg_file03_test03() {
        // given fileName
        String fileName = "file3.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.empty());

        // then verify results
        assertEquals(2, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 4.01);
    }


    /**
     * file4 - file4.txt
     */
    @Test
    public void executeAlg_file04_test01() {
        // given fileName
        String fileName = "file4.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(0));

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);
        // check hospitals
        assertEquals(new Point(1,1), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(9,1), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(5,1), closestHospitalAlg.getHopistalList().get(2).getLocation());
    }
    @Test
    public void executeAlg_file04_test02() {
        // given fileName
        String fileName = "file4.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(1));

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);
        // check hospitals
        assertEquals(new Point(2,1), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(9,1), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(5,1), closestHospitalAlg.getHopistalList().get(2).getLocation());
    }
    @Test
    public void executeAlg_file04_test03() {
        // given fileName
        String fileName = "file4.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.empty());

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);
    }


    /**
     * file5 - file5.txt
     */
    @Test
    public void executeAlg_file05_test01() {
        // given fileName
        String fileName = "file5.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(0));
//        closestHospitalAlg.executeAlg(fileName, Optional.empty());

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);
        // check hospitals
        assertEquals(new Point(2,1), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(7,2), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(4,3), closestHospitalAlg.getHopistalList().get(2).getLocation());
    }

    // do not chech hospitals (due to first random)
    @Test
    public void executeAlg_file05_test02() {
        // given fileName
        String fileName = "file5.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
//        closestHospitalAlg.executeAlg(fileName, Optional.of(3));
        closestHospitalAlg.executeAlg(fileName, Optional.empty());

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);    // optimal is 1.0, so worst should be <=2.0
    }

    // select (1,2) as first hospital, so hospitals should be (1,2),(7,2),(4,1)
    @Test
    public void executeAlg_file05_test03() {
        // given fileName
        String fileName = "file5.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(3));

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);    // optimal is 1.0, so worst should be <=2.0
        // check hospitals
        assertEquals(new Point(1,2), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(7,2), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(4,1), closestHospitalAlg.getHopistalList().get(2).getLocation());
    }

    // select (3,2) as first hospital, so hospitals should be (2,3),(7,2),(1,2)
    @Test
    public void executeAlg_file05_test04() {
        // given fileName
        String fileName = "file5.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(5));

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);    // optimal is 1.0, so worst should be <=2.0
        // check hospitals
        assertEquals(new Point(3,2), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(7,2), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(1,2), closestHospitalAlg.getHopistalList().get(2).getLocation());
    }

    // select (2,3) as first hospital, so hospitals should be (2,3),(7,2),(4,1)
    @Test
    public void executeAlg_file05_test05() {
        // given fileName
        String fileName = "file5.txt";

        // when executing ClosestHospitalAlg.executeAlg(...)
        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg(fileName, Optional.of(10));

        // then verify results
        assertEquals(3, closestHospitalAlg.getHopistalList().size());             // check hospital list size
        assertTrue(closestHospitalAlg.getGlobalFarthestCity().getDistance() < 2.01);    // optimal is 1.0, so worst should be <=2.0
        // check hospitals
        assertEquals(new Point(2,3), closestHospitalAlg.getHopistalList().get(0).getLocation());
        assertEquals(new Point(7,2), closestHospitalAlg.getHopistalList().get(1).getLocation());
        assertEquals(new Point(4,1), closestHospitalAlg.getHopistalList().get(2).getLocation());
    }


}
