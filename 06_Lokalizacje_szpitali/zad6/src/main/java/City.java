public class City {

    private Point location;
    private City closestHospital;
    private City farthestCity;
    private double closestHospitalDistance;
    private double fathestCityDistance;


    public City(Point location) {
        this.location = location;
    }
    public City() {}


    public Point getLocation() {
        return location;
    }
    public void setFarthestCity(City farthestCity) {
        this.farthestCity = farthestCity;
    }
    public void setFathestCityDistance(double fathestCityDistance) {
        this.fathestCityDistance = fathestCityDistance;
    }
    public void setClosestHospital(City closestHospital) {
        this.closestHospital = closestHospital;
    }
    public double getClosestHospitalDistance() {
        return closestHospitalDistance;
    }
    public void setClosestHospitalDistance(double closestHospitalDistance) {
        this.closestHospitalDistance = closestHospitalDistance;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("City{");
        sb.append("location=" + location);
        if(closestHospital == null){
            sb.append(", closestHospital=null");
        } else {
            sb.append(", closestHospital=" + closestHospital.getLocation());
        }

        if(farthestCity == null){
            sb.append(", farthestCity=null");
        } else {
            sb.append(", farthestCity=" + farthestCity.getLocation());
        }

        sb.append(", closestHospitalDistance=" + closestHospitalDistance);
        sb.append(", fathestCityDistance=" + fathestCityDistance);
        sb.append('}');
        return sb.toString();
    }


}
