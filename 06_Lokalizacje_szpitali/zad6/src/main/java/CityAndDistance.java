public class CityAndDistance {

    private City city;
    private double distance;


    public CityAndDistance(City city, double distance) {
        this.city = city;
        this.distance = distance;
    }


    public City getCity() {
        return city;
    }
    public double getDistance() {
        return distance;
    }


}
