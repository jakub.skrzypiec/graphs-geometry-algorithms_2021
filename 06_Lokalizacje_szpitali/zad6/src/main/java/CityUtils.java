import org.apache.log4j.Logger;

import java.util.List;
import java.util.Random;

public class CityUtils {

    private static Logger logger = Logger.getLogger(CityUtils.class);

    /**
     * method to select a random City from List<City>
     */
    public static City randomCityFromList(List<City> cityList){
        logger.info("SELECTING RANDOM CITY FROM CITYLIST...");
        Random random = new Random();
        int randomIndex = random.ints(0, cityList.size())
                .findFirst()
                .getAsInt();

        City randomCity = cityList.get(randomIndex);
        logger.debug("\tSelected random index: " + randomIndex);
        logger.info("\tSELECTED RANDOM CITY: " + randomCity);
        return randomCity;
    }

    /**
     * method to calculate distance between 2 Cities
     *
     *  public static double calculateDistance(Point p1, Point p2) {
     *         return Math.sqrt(Math.pow(p2.getX() - p1.getX(), 2) + Math.pow(p2.getY() - p1.getY(), 2));
     *     }
     */
    public static double calculateDistance(City city1, City city2){
        return Math.sqrt(Math.pow(city2.getLocation().getX() - city1.getLocation().getX(), 2) +
                Math.pow(city2.getLocation().getY() - city1.getLocation().getY(), 2));
    }

    /**
     * method to print out CityList
     */
    public static void logCityList(List<City> cityList){
        for(int i=0; i<cityList.size(); i++){
            logger.debug("\t--[" + i + "] " + cityList.get(i));
        }
    }

    /**
     * CityUtils.caulcateDistances(cities, firstHospital);
     * method to calculate distances between hospital and each city from List<City>
     */
    public static void calculateDistances(City hospital, List<City> cityList) {
        logger.debug("Distances before:");
        logCityList(cityList);

        for(City city : cityList){
            // calculate dist between City and Hospital
            double distanceTmp = calculateDistance(hospital, city);

            // if distance is lower -> change city.closestHospital to current Hospital
            if(distanceTmp < city.getClosestHospitalDistance()) {
                city.setClosestHospitalDistance(distanceTmp);
                city.setClosestHospital(hospital);
            }

        }
        logger.debug("Distances after:");
        logCityList(cityList);
    }

    /**
     * method to calculate global maxDistance to hospital - iterate over cityList, for each city check distance to
     * closest hospital -> take global max from these values
     */
    public static CityAndDistance updateGlobalFarthestCity(List<City> cityList, City currentHospital){
        logger.info("UPDATING GLOBAL FARTHEST CITY...");

        City tmpFarthestCity = new City();
        double tmpMaxMinDistanceToHospital = Double.MIN_VALUE;

        for(City city : cityList) {
            if (city.getClosestHospitalDistance() > tmpMaxMinDistanceToHospital) {
                tmpMaxMinDistanceToHospital = city.getClosestHospitalDistance();
                tmpFarthestCity = city;
            }
        }

        logger.info("GLOBAL FARTHEST CITY: " );
        logger.info("\tdistance: " + tmpMaxMinDistanceToHospital);
        logger.info("\t" + tmpFarthestCity);

        currentHospital.setFathestCityDistance(tmpMaxMinDistanceToHospital);
        currentHospital.setFarthestCity(tmpFarthestCity);

        return new CityAndDistance(tmpFarthestCity, tmpMaxMinDistanceToHospital);
    }



}
