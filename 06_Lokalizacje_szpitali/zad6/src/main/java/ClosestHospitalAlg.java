import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClosestHospitalAlg {

    private CityAndDistance globalFarthestCity;    // city that has the biggest (global) distance to it's closest hospital
    private List<City> hopistalList = new ArrayList<>();    // list of hospitals

    private int k;
    private List<City> cities;


    public ClosestHospitalAlg () {};


    private static Logger logger = Logger.getLogger(Zad6_Application.class);

    public void executeAlg(String fileName, Optional<Integer> firstHospitalIndex) {
        // read k, cityList from file
        k = FileUtils.fileToKNumber(fileName);
        cities = FileUtils.fileToCityList(fileName);

        // 1st hospital - select random City
        City firstHospital = CityUtils.randomCityFromList(cities);
        // mock selection (if 2nd arg passed)
        if(firstHospitalIndex.isPresent()){
            firstHospital = cities.get(firstHospitalIndex.get());
            logger.debug("mock random city (1st hospital): " + firstHospital);
        }
        //
        hopistalList.add(firstHospital);

        // 2) calculate distances between Hospital & List<City>
        CityUtils.calculateDistances(firstHospital, cities);

        // 3) update global fathest city (next hospital if needed)
        globalFarthestCity = CityUtils.updateGlobalFarthestCity(cities, firstHospital);

        // for 2..k do: place hospital, update distances, find fathrestCityFromHospital...
        for(int j=2; j<=k; j++){
            logger.info("PLACING [" + j + "] HOSPITAL...");

            // 1) place Hospital & add it to hospitalList
            City currentHospital = globalFarthestCity.getCity();
            hopistalList.add(currentHospital);

            // 2) calculate distances between Hospital & List<City>
            CityUtils.calculateDistances(currentHospital, cities);

            // 3) update global fathest city (next hospital if needed)
            globalFarthestCity = CityUtils.updateGlobalFarthestCity(cities, currentHospital);
        }

        // log result hospitals
        logger.info("PLACED HOSPITALS:");
        CityUtils.logCityList(hopistalList);
    }


    public List<City> getHopistalList() {
        return hopistalList;
    }
    public List<City> getCities() {
        return cities;
    }
    public CityAndDistance getGlobalFarthestCity() {
        return globalFarthestCity;
    }


}
