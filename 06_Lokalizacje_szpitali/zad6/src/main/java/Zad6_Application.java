import org.apache.log4j.BasicConfigurator;

import java.util.Optional;


public class Zad6_Application {

    public static void main(String[] args) {
        BasicConfigurator.configure();  // configure the logging (default)

        ClosestHospitalAlg closestHospitalAlg = new ClosestHospitalAlg();
        closestHospitalAlg.executeAlg("file1.txt", Optional.of(1));
    }


}
