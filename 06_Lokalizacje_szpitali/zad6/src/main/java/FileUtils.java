import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    private static Logger logger = Logger.getLogger(FileUtils.class);

    /**
     * method that reads k number from file
     */
    public static int fileToKNumber(String fileName) {
        int  k = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            logger.info("READING K-NUMBER FROM FILE...");
            if ((line = br.readLine()) != null) {
                k = Integer.parseInt(line);
            }
            logger.info("\tk: " + k);
        } catch(IOException e){
            e.printStackTrace();
        }
        return k;
    }

    /**
     * method that reads from file to List<City> from format:
     *  -1st line is a single number: k (number of cities),
     *  -rest of the lines are cities (point coordinates)
     *
     *  e.g. : (k=2, 4 cities [points])
     *      2
     *      1,1
     *      2,2
     *      3,3
     *      4,4
     */
    public static List<City> fileToCityList(String fileName) {
        List<City> cityList = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            String[] lineArr;
            logger.info("READING CITIES FROM FILE...");

            line = br.readLine(); // skip first line
            while ((line = br.readLine()) != null) {
                lineArr = line.split(",");
                if(lineArr.length != 2){
                    throw new IllegalArgumentException("Incorrect file format!");
                }

                if (lineArr.length == 2){
                    double x = Double.parseDouble(lineArr[0]);
                    double y = Double.parseDouble(lineArr[1]);
                    Point cityLocation = new Point(x,y);
                    // create new City, initialize its closest and farthest city distances
                    City newCity = new City(cityLocation);
                    newCity.setClosestHospitalDistance(Double.MAX_VALUE);
                    newCity.setFathestCityDistance(-Double.MAX_VALUE);
                    cityList.add(newCity);
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }

        logger.info("CREATED CITY LIST (size " + cityList.size() + "):");
        for(int i=0; i<cityList.size(); i++){
            logger.info("\t--[" + i + "] " + cityList.get(i));
        }

        return cityList;
    }
}
