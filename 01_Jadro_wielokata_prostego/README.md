# zad01
## Polygon kernell 
### Check whether polygon kernell exists

### Input
    - Polygon (set of points, where p1-p2 creates edge)
### Output
    - Boolean (does the polygon kernell exist)

### Example
![xxx](IMG/1.png "xxx")
