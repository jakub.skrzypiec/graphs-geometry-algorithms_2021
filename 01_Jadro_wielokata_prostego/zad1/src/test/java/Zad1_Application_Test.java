import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Zad1_Application_Test {

    public void test_isKernellNotNull_drawPolygon(List<Integer> xList, List<Integer> yList, boolean assertExpected){
        // Given args...
        // When
        List<Point> pointList = Zad1_Application.preparePointList(xList, yList);        // prepare list of Points
        List<List<Point>> extremaList = Zad1_Application.findLocalExtrema(pointList);   // find local extrema
        boolean isKernellNotNull = Zad1_Application.polygonKernellExists(extremaList);  // check polygon kernell
        Zad1_Application.drawPoints(pointList, isKernellNotNull);   // draw polygon
        // Then
        assertEquals(assertExpected, isKernellNotNull);
    }


    @Test
    public void test_checkPolygonKernell_polygon01(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,4,4,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,4,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon02(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,0,1,1,2,2,3,3,4,4,5,5));
        List<Integer> yList = new ArrayList<>(Arrays.asList(3,1,1,2,2,0,0,2,2,1,1,3));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon03(){
        DrawPolygon.scale = 80;
        DrawPolygon.ySize = 800;
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,1,2,3,6,8,5,3,3,2,2,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,3,1,3,1,7,7,5,7,6,8,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon04(){
        DrawPolygon.scale = 80;
        DrawPolygon.ySize = 800;
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,1,2,3,6,8,5,3,3,2,2,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,3,1,3,1,7,7,5,7,2,8,4));
        test_isKernellNotNull_drawPolygon(xList, yList, false);
    }

    @Test
    public void test_checkPolygonKernell_polygon05(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,4,4,2,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,4,2,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon06(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,3,4,2,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,4,2,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon07(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,4,3,2,1));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,4,2,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon08(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,2,4,4,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,2,0,4,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon09(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,2,4,4,3,2,1,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,2,0,4,3,4,3,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon10(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,2,4,4,3,2,1,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,2,0,4,2,4,3,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon11(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,1,4,4,3,2,1,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,2,0,4,1,4,3,4));
        test_isKernellNotNull_drawPolygon(xList, yList, false);
    }

    @Test
    public void test_checkPolygonKernell_polygon12(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,2,1,4,4,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,2,0,4,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon13(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,4,4,1,2,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,4,2,4,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon14(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,3,1,4,4,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,2,0,0,4,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon15(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,4,4,2,3,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,4,4,1,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }

    @Test
    public void test_checkPolygonKernell_polygon16(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,2,1,4,4,2,3,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,2,1,5,5,3,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }


    // new test cases
    @Test
    public void test_checkPolygonKernell_polygon_01(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,3,3,2,1));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,2,1,4,3,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }
    @Test
    public void test_checkPolygonKernell_polygon_02(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,3,4,4,3,2,1));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,2,2,1,4,4,3,4));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }
    @Test
    public void test_checkPolygonKernell_polygon_03(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,3,3,4,4,3,3,2,2,1));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,1,2,2,4,4,3,3,2,2));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }
    @Test
    public void test_checkPolygonKernell_polygon_04(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,3,2));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,2,2,2));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }
    @Test
    public void test_checkPolygonKernell_polygon_05(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,3,2,3,2,1,2));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,2,1,2,3,2,3,2));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }
    @Test
    public void test_checkPolygonKernell_polygon_06(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,3,4,4,3,2,1));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,1,2,1,3,3,2,3));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }
    @Test
    public void test_checkPolygonKernell_polygon_07(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,2,3,3,6,6,5,5,4,4,1));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,1,4,4,1,1,5,5,2,2,5,5));
        test_isKernellNotNull_drawPolygon(xList, yList, false);
    }
    @Test
    public void test_checkPolygonKernell_polygon_08(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,3,4,5,3));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,1,2,1,1,3));
        test_isKernellNotNull_drawPolygon(xList, yList, true);
    }
    @Test
    public void test_checkPolygonKernell_polygon_09(){
        List<Integer> xList = new ArrayList<>(Arrays.asList(1,2,3,3,2,1));
        List<Integer> yList = new ArrayList<>(Arrays.asList(1,4,1,3,0,3));
        test_isKernellNotNull_drawPolygon(xList, yList, false);
    }

}
