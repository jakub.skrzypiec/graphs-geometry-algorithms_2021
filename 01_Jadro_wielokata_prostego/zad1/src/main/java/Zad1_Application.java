import javafx.application.Application;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zad1_Application {

    public static void main(String[] args){
        List<Integer> xList = new ArrayList<>(Arrays.asList(0,2,3,4,5,5,4,3,2,1,0));
        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,2,1,1,5,5,5,3,4,4));
        DrawPolygon.scale = 80;
        DrawPolygon.ySize = 800;

        // create List of Points
        List<Point> pointList = preparePointList(xList, yList);
        for(Point point : pointList){
            System.out.println(point);
        }

        // check for local max and local min
        List<List<Point>> extremaList = findLocalExtrema(pointList);
//        List<Point> localMaximumsList = extremaList.get(0);
//        List<Point> localMinimumsList = extremaList.get(1);
//        Point globalMax = extremaList.get(2).get(0);
//        Point globalMin = extremaList.get(3).get(0);

        // check kenrell
        boolean isKernellNotNull = polygonKernellExists(extremaList);

        // draw polygon
        drawPoints(pointList, isKernellNotNull);
    }


    // method to prepare List of Points
    public static List<Point> preparePointList(List<Integer> xList, List<Integer> yList) {
        List<Point> pointList = new ArrayList<>();
        if (xList.size()>0 && yList.size()>0 && xList.size()==yList.size()) {
            // crete Point objects
            for (int i=0; i<xList.size(); i++){
                pointList.add(new Point(xList.get(i), yList.get(i)));
            }

            // set next&prev relations
            if (pointList.size()>1){
                Point firstPoint = pointList.get(0);
                Point lastPoint = pointList.get(pointList.size()-1);

                firstPoint.setPrev(lastPoint);
                lastPoint.setNext(firstPoint);
                for (int i=0; i<pointList.size()-1; i++) {
                    Point currentPoint = pointList.get(i);
                    Point nextPoint = pointList.get(i+1);
                    currentPoint.setNext(nextPoint);
                    nextPoint.setPrev(currentPoint);
                }
            }
        }
        return pointList;
    }

    // draw points
    public static void drawPoints(List<Point> pointList, boolean isKernellNotNull){
        if(!isKernellNotNull){
            DrawPolygon.polygonColor = Color.FIREBRICK; // red color when kernell does not exist, green when it does
        }
        DrawPolygon.pointList = pointList;
        Application.launch(DrawPolygon.class);
    }

    // check orientation of 3 points -- (-1) means LEFT, (0) means COLLINEAR, (1) means RIGHT
    public static int checkOrientation(Point curr) {
        Point prev = curr.getPrev();
        Point next = curr.getNext();
        int value = (curr.getY() - prev.getY()) * (next.getX() - curr.getX()) - (curr.getX() - prev.getX()) * (next.getY() - curr.getY());
        if(value < 0){
            return -1;  // left
        } else if (value > 0) {
            return 1;   // right
        } else {
            return 0;
        }
    }

    public static boolean isLocalMax(Point curr){
        Point prev = curr.getPrev();
        Point next = curr.getNext();
        if (prev.getY() < curr.getY() && curr.getY() > next.getY()) {
            return true;
        } else if (prev.getY() < curr.getY() && curr.getY()==next.getY() && next.getY() > next.getNext().getY()) {
            return true;
        }else {
            return false;
        }
    }

    public static boolean isLocalMin(Point curr){
        Point prev = curr.getPrev();
        Point next = curr.getNext();
        if (prev.getY() > curr.getY() && curr.getY() < next.getY()) {
            return true;
        } else if (prev.getY() > curr.getY() && curr.getY()==next.getY() && next.getY() < next.getNext().getY()){
                return true;
        } else {
            return false;
        }
    }

    // method to find local extrema (local maximums and minimums)
    public static List<List<Point>> findLocalExtrema(List<Point> pointList) {
        List<Point> localMaximumsList = new ArrayList<>();
        List<Point> localMinimumsList = new ArrayList<>();
        List<Point> yMaxPointList = new ArrayList<>();
        List<Point> yMinPointList = new ArrayList<>();

        // find absolute yMax and yMin
        Point yMaxPoint = pointList.get(0);
        Point yMinPoint = pointList.get(0);
        for (int i=1; i<pointList.size(); i++) {
            int yTmp = pointList.get(i).getY();
            if (yTmp > yMaxPoint.getY()) {
                yMaxPoint = pointList.get(i);
            } else if (yTmp < yMinPoint.getY()) {
                yMinPoint = pointList.get(i);
            }
        }
        yMaxPointList.add(yMaxPoint);
        yMinPointList.add(yMinPoint);

        for (int i=0; i<pointList.size(); i++){
            Point currentPoint = pointList.get(i);

            int currOrientation = checkOrientation(currentPoint);
            if (currOrientation == -1){     // left
                System.out.println("LEFT " + currentPoint);
            } else if (currOrientation == 1) {  // right
                System.out.println("RIGHT " + currentPoint);
                // possible localMin / localMax
                if (isLocalMax(currentPoint)) {
                    localMaximumsList.add(currentPoint);
                }
                if (isLocalMin(currentPoint)) {
                    localMinimumsList.add(currentPoint);
                }

            } else if (currOrientation == 0) {  // collinear
                // check next point
                System.out.println("COLLINEAR " + currentPoint);
            }

        }
        System.out.println("global max:" + yMaxPoint);
        System.out.println("global min:" + yMinPoint);
        System.out.println("local max list: " + localMaximumsList);
        System.out.println("local min list: " + localMinimumsList);

        List<List<Point>> extremaList = new ArrayList<>();
        extremaList.addAll(Arrays.asList(localMaximumsList, localMinimumsList, yMaxPointList, yMinPointList));
        return extremaList;
    }

    // method to check wheter polygon kernell exists (is that kernell a null)
    public static boolean polygonKernellExists(List<List<Point>> extremaList) {
        List<Point> localMaximums = extremaList.get(0);
        List<Point> localMinimums = extremaList.get(1);
        Point globalMax = extremaList.get(2).get(0);
        Point globalMin = extremaList.get(3).get(0);

        Point maxLocalMaximum = null;
        Point minLocalMinimum = null;
        // find max local maximum and min local minimum
        if(localMaximums!=null && localMaximums.size()>0){
            maxLocalMaximum = localMaximums.get(0);
            if(localMaximums.size()>1){
                for(int i=0; i<localMaximums.size(); i++){
                    Point tmp = localMaximums.get(i);
                    if(tmp.getY() > maxLocalMaximum.getY()){
                        maxLocalMaximum = tmp;
                    }
                }
            }
        }
        if(localMinimums!=null && localMinimums.size()>0){
            minLocalMinimum = localMinimums.get(0);
            if(localMinimums.size()>1){
                for(int i=0; i<localMinimums.size(); i++){
                    Point tmp = localMinimums.get(i);
                    if(tmp.getY() < minLocalMinimum.getY()){
                        minLocalMinimum = tmp;
                    }
                }
            }
        }

        // if there is no local max - set global min as maxLocalMax
        if(localMaximums==null || localMaximums.size()==0){
            maxLocalMaximum = globalMin;
        }

        // if there is no local min - set global max as minLocalMin
        if(localMinimums==null || localMinimums.size()==0){
            minLocalMinimum = globalMax;
        }

        // check whether polygon kernell exists
        if(minLocalMinimum.getY() >= maxLocalMaximum.getY()){
            System.out.println("isKernellNotnull: true\n");
            return true;    // polygon kernell does exist
        } else {
            System.out.println("isKernellNotnull: false\n");
            return false;   // polygon kernell does NOT exist
        }
    }


}
