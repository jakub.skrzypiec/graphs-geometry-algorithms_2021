import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;

import java.util.List;


public class DrawPolygon extends Application {

    public static List<Point> pointList;
    public static int scale = 100;
    public static int margin = 20;
    public static int xSize = 1200;
    public static int ySize = 600;
    public static Color polygonColor = Color.GREEN;


    public static Double[] pointListToDoubleArray(){
        int size = pointList.size();
        Double[] doubleArr = new Double[2*size];

        for(int i=0; i<size; i++){
            doubleArr[2*i] = (double)pointList.get(i).getX()*scale+margin;      // defalt: *100+20
            doubleArr[2*i+1] = (double)pointList.get(i).getY()*scale+margin;
        }
        return doubleArr;
    }

    public static void showStage(){
        Stage stage = new Stage();

        // Creating a Polygon
        Polygon polygon = new Polygon();
        polygon.setFill(polygonColor);

        // Adding coordinates to the polygon
        polygon.getPoints().addAll(pointListToDoubleArray());

        // Creating a Group object
        Group root = new Group(polygon);

        // Creating a scene object
        Scene scene = new Scene(root, xSize, ySize);    // default size: 1200x600

        // Setting title to the Stage
        stage.setTitle("Drawing a Polygon");

        // Adding scene to the stage
        stage.setScene(scene);

        // Mirror - Y axis
        root.setScaleY(-1);

        // Displaying the contents of the stage
        stage.show();
    }

    @Override
    public void start(Stage stage) {
        showStage();
    }

    public static void main(String args[]){
//        List<Integer> xList = new ArrayList<>(Arrays.asList(0,2,1,4,4,2,3,0));
//        List<Integer> yList = new ArrayList<>(Arrays.asList(0,0,2,0,5,5,3,4));
//        DrawPolygon.pointList = Zad1_Application.preparePointList(xList, yList);
//        Application.launch(DrawPolygon.class);
    }


}
