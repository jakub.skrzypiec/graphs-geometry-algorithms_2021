public class Point {

    private int x;
    private int y;
    private Point next;
    private Point prev;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public Point getNext() {
        return next;
    }
    public void setNext(Point next) {
        this.next = next;
    }
    public Point getPrev() {
        return prev;
    }
    public void setPrev(Point prev) {
        this.prev = prev;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", next=(" + next.getX() + "," + next.getY() + ")" +
                ", prev=(" + prev.getX() + "," + prev.getY() + ")" +
                '}';
    }
}
